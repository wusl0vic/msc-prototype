#!/usr/bin/env bash
export SPARK_LOCAL_IP=`awk 'NR==1 {print $1}' /etc/hosts`
cd /usr/local/spark
./bin/spark-shell \
    --master spark://spark-master:7077 \
    --conf spark.driver.host=${SPARK_LOCAL_IP} \
    --properties-file /spark-defaults.conf \
    --jars /spark-cassandra-connector_2.11-2.3.1.jar \
    --conf spark.cassandra.connection.host=${CASSANDRA_PORT_7001_TCP_ADDR} \
    "$@"