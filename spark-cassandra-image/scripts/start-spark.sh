#!/usr/bin/env bash

# Set default HDFS user if not set
if [[ -z "${HDFS_USER}" ]]; then
  export HDFS_USER=spark
fi

if [[ "${1}" = 'master' ]]; then
  # start cassandra instance
  echo "starting cassandra ..."
  cassandra -D
  status = $?
  if [ $status -ne 0 ]; then
    echo "Failed to start cassandra: $status"
    exit $status
  fi
  echo "done starting cassandra ..."
  # Start Spark Master
  spark-class org.apache.spark.deploy.master.Master -h $(hostname)
elif [[ "${1}" = 'worker' ]]; then
  # Wait for the Spark Master/Hadoop Namenode to start
  while ! nc -z $2 7077 ; do
    sleep 2;
  done;
  # Start Spark Worker
  spark-class org.apache.spark.deploy.worker.Worker spark://$2:7077
else
  echo "Invalid command '${1}'" >&2
  exit 1
fi