FROM ubuntu:18.04

# install supervisord to start child processes
RUN apt-get update && apt-get install -y supervisor && mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# install java, curl, gnupg2
RUN apt-get update && apt-get install -y openjdk-8-jdk curl gnupg2 netcat-openbsd tzdata acl unzip

# download and install spark
RUN curl -s https://archive.apache.org/dist/spark/spark-2.3.1/spark-2.3.1-bin-hadoop2.7.tgz | tar -xz -C /usr/local/
RUN cd /usr/local && ln -s spark-2.3.1-bin-hadoop2.7 spark

# install cassandra
ENV TZ Europe/Vienna
COPY cassandra-keyfile /keyfile
RUN echo "deb http://www.apache.org/dist/cassandra/debian 22x main" | tee -a /etc/apt/sources.list.d/cassandra.sources.list
RUN apt-key add keyfile
RUN apt-get update && apt-get install -y cassandra

# install livy
RUN curl -s http://mirror.klaus-uwe.me/apache/incubator/livy/0.5.0-incubating/livy-0.5.0-incubating-bin.zip --output /tmp/livy.zip
RUN unzip /tmp/livy.zip -d /usr/local
RUN rm /tmp/livy.zip
RUN cd /usr/local && ln -s livy-0.5.0-incubating-bin livy

# copy start scripts
COPY scripts/start-spark.sh /start-spark.sh
COPY scripts/start-master.sh /start-master.sh
COPY scripts/start-worker.sh /start-worker.sh
COPY scripts/spark-shell.sh /spark-shell.sh
COPY scripts/spark-cassandra-connector_2.11-2.3.1.jar /spark-cassandra-connector_2.11-2.3.1.jar
COPY scripts/spark-defaults.conf /spark-defaults.conf
COPY scripts/start-livy.sh /start-livy.sh
COPY sample-data.csv /sample-data.csv
COPY sample-data.cql /sample-data.cql

# setup environment for history server
RUN mkdir /tmp/spark-events
RUN chmod g+rwxs /tmp/spark-events
RUN chmod o+rs /tmp/spark-events
# RUN chmod g+s /tmp/spark-events
# RUN setfacl -d -m g::rwx /tmp/spark-events
# RUN setfacl -d -m o::rx /tmp/spark-events

# TODO: fix user permissions to get read access on /tmp/spark-events on the host
# RUN addgroup --gid 1042 history-group
# RUN adduser --disabled-password --gecos "" --force-badname --ingroup history-group history-user

# configure spark
ENV SPARK_HOME /usr/local/spark
ENV SPARK_MASTER_OPTS="-Dspark.driver.port=7001 -Dspark.fileserver.port=7002 -Dspark.broadcast.port=7003 -Dspark.replClassServer.port=7004 -Dspark.blockManager.port=7005 -Dspark.executor.port=7006 -Dspark.ui.port=4040 -Dspark.broadcast.factory=org.apache.spark.broadcast.HttpBroadcastFactory"
ENV SPARK_WORKER_OPTS=$SPARK_MASTER_OPTS
ENV SPARK_MASTER_PORT 7077
ENV SPARK_MASTER_WEBUI_PORT 8080
ENV SPARK_WORKER_PORT 8888
ENV SPARK_WORKER_WEBUI_PORT 8081
ENV HDFS_USER spark
ENV PATH=$PATH:$SPARK_HOME/bin

# configure cassandra
ENV CASSANDRA_CONFIG /etc/cassandra

RUN sed -ri ' \
		s/^(rpc_address:).*/\1 0.0.0.0/; \
	' "$CASSANDRA_CONFIG/cassandra.yaml"

COPY cassandra-config.sh /cassandra-config.sh
ENTRYPOINT ["/cassandra-config.sh"]

VOLUME /var/lib/cassandra

EXPOSE 4040 7000 7001 7002 7003 7004 7005 7006 7077 7199 8080 8081 8888 9042 9160

CMD ["/usr/bin/supervisord"]