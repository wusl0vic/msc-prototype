package dore.sparkexecutor;

import com.dynatrace.msc.dore.query.DirectExecutor;
import com.dynatrace.msc.dore.query.SparkExecutor;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CONF;

public class DirectExecutorTest {

    private static final String KP = "kp_test";
    private static final String TABLE = "usersession";

    public static void main(String[] args) throws Exception {
        var conf = SPARK_CONF;
        conf.set("spark.executor.instances", "3")
            .set("spark.executor.cores", "2");
        SparkExecutor executor = new DirectExecutor("spark://localhost:7077", conf);
        System.out.println("Calculating top 10 cities CMS ...");
        var cms = executor.topKCountMinSketch(KP, TABLE, "city", 10, 200, 0.05, 0.95).get();
        cms.forEach(System.out::println);

        System.out.println("Calculating top 10 cities SpaceSaving ...");
        cms = executor.topKSpaceSaving(KP, TABLE, "city", 10, 200).get();
        cms.forEach(System.out::println);

        System.out.println("Calculating top 10 cities exact ...");
        cms = executor.topKExact(KP, TABLE, "city", 10).get();
        cms.forEach(System.out::println);
    }

}
