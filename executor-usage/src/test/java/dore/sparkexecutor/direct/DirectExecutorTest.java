package dore.sparkexecutor.direct;

import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.DirectExecutor;
import com.dynatrace.msc.dore.query.SparkExecutor;
import com.google.common.base.Stopwatch;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CONF;

public class DirectExecutorTest {

    private static final String KP = "kp_test";
    private static final String TABLE = "usersession";
    private static final String MASTER_URL = "spark://localhost:7077";

    private static SparkExecutor executor;

    @BeforeClass
    public static void beforeClass() throws Exception {
        var conf = SPARK_CONF;
        conf.set("spark.executor.instances", "3")
            .set("spark.executor.cores", "2");
        executor = new DirectExecutor(MASTER_URL, conf);
//        executor.count(KP, TABLE).get(); // trigger caching of the dataset
    }

    @AfterClass
    public static void afterClass() throws Exception {
        executor.close();
    }

    @Test
    public void testTopKFunctions() throws Exception {
        Stopwatch sw = Stopwatch.createStarted();
        var counts = executor.topKExact(KP, TABLE, "city", 10).get();
        sw.stop();
        System.out.println("Exact:");
        counts.forEach(System.out::println);
        System.out.println("took " + sw);
        System.out.println();

        sw.reset(); sw.start();
        counts = executor.topKCountMinSketch(KP, TABLE, "city", 10, 400, 0.05, 0.9).get();
        sw.stop();
        System.out.println("CMS:");
        counts.forEach(System.out::println);
        System.out.println("took " + sw);
        System.out.println();

        sw.reset(); sw.start();
        counts = executor.topKSpaceSaving(KP, TABLE, "city", 10, 400).get();
        sw.stop();
        System.out.println("SpaceSaving:");
        counts.forEach(System.out::println);
        System.out.println("took " + sw);
    }

    @Test
    public void testTopKParallel() {
        var now = Instant.now();

        var f1 = executor.topKExact(KP, TABLE, "city", 10)
                .thenAcceptAsync(top -> printTopResult(now, top, "Exact"));
        var f2 = executor.topKCountMinSketch(KP, TABLE, "city", 10, 400, 0.05, 0.95)
                .thenAcceptAsync(top -> printTopResult(now, top, "CMS"));
        var f3 = executor.topKSpaceSaving(KP, TABLE, "city", 10, 400)
                .thenAcceptAsync(top -> printTopResult(now, top, "SpaceSaving"));

        CompletableFuture.allOf(f1, f2, f3).join();
    }

    private void printTopResult(Instant start, List<Counter<String>> top, String algo) {
        var end = Instant.now();
        var elapsed = Duration.between(start, end).toMillis();
        System.out.println(algo + ":");
        System.out.println(top);
        System.out.println("took " + elapsed + " ms");
    }

}
