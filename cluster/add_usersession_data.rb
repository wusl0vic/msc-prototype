require 'cassandra'
require 'json'
require 'rest-client'
require 'uri'

# cluster = Cassandra.cluster

# cluster.each_host do |host|
#     puts "Host #{host.ip}: id=#{host.id} datacentre=#{host.datacenter} rack=#{host.rack}"
# end

keyspace = 'kp_test'
table = 'usersession'
# session = cluster.connect(keyspace)

url = "https://demo.dev.dynatracelabs.com/api/v1/userSessionQueryLanguage/table"
token = "YkX2DPbuT3iXlNI-iYLc2"
query = "select json from usersession limit 5000"
startTs = (DateTime.now - 7).strftime('%Q')
endTs = DateTime.now.strftime('%Q')

response = RestClient.get "#{url}?Api-Token=#{token}&query=#{URI::encode(query)}&startTimestamp=#{startTs}&endTimestamp=#{endTs}"

puts "got usersession data from API ..."

res_json = JSON.parse(response)

puts "parsed response ..."

filename = "insert.cql"
f = File.open(filename, 'w')

res_json['values'].each do |col|
    val = JSON.parse(col[0])
    row = {
        :tenantId => val['tenantId'],
        :userSessionId => val['userSessionId'],
        :startTime => val['startTime'],
        :endTime => val['endTime'],
        :duration => val['duration'],
        :internalUserId => val['internalUserId'],
        :userType => val['userType'],
        :applicationType => val['applicationType'],
        :bounce => val['bounce'],
        :newUser => val['newUser'],
        :userActionCount => val['userActionCount'],
        :totalErrorCount => val['totalErrorCount'],
        :ip => val['ip'],
        :continent => val['continent'],
        :country => val['country'],
        :region => val['region'], # remove if encoding issues are not fixed easily
        :city => val['city'],
        :browserType => val['browserType'],
        :browserFamily => val['browserFamily'],
        :osFamily => val['osFamily'],
        :osVersion => val['osVersion'],
        :userId => val['userId'],
        :screenHeight => val['screenHeight'],
        :screenWidth => val['screenWidth'],
        :isp => val['isp'],
        :clientType => val['clientType'],
        :endReason => val['endReason']
    }
    row_str = row.to_json
    row_js = JSON.parse(row_str)
    # puts JSON.pretty_generate(row_js)

    escaped = row_str.gsub("'", "''")
    statement = "INSERT INTO #{keyspace}.#{table} JSON '#{escaped}';\n"
    # puts stmnt
    # session.execute(statement)
    f.write(statement)
end
f.close