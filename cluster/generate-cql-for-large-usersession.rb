require 'json'
require 'uri'

keyspace = 'kp_test'
table = 'usersession'

export_file_name = 'RESTData-2019-07-16.json'
output_file = File.open('insert-big.cql', 'w')

puts "generating output file 'insert-big.cql' ..."

File.open(export_file_name, 'r') do |file_handle|
    file_handle.each_line do |line|
        val = JSON.parse(line)
        row = {
            :tenantId => val['tenantId'],
            :userSessionId => val['userSessionId'],
            :startTime => val['startTime'],
            :endTime => val['endTime'],
            :duration => val['duration'],
            :internalUserId => val['internalUserId'],
            :userType => val['userType'],
            :applicationType => val['applicationType'],
            :bounce => val['bounce'],
            :newUser => val['newUser'],
            :userActionCount => val['userActionCount'],
            :totalErrorCount => val['totalErrorCount'],
            :ip => val['ip'],
            :continent => val['continent'],
            :country => val['country'],
            :region => val['region'],
            :city => val['city'],
            :browserType => val['browserType'],
            :browserFamily => val['browserFamily'],
            :osFamily => val['osFamily'],
            :osVersion => val['osVersion'],
            :userId => val['userId'],
            :screenHeight => val['screenHeight'],
            :screenWidth => val['screenWidth'],
            :isp => val['isp'],
            :clientType => val['clientType'],
            :endReason => val['endReason']
        }
        row_str = row.to_json
        row_js = JSON.parse(row_str)

        escaped = row_str.gsub("'", "''")
        statement = "INSERT INTO #{keyspace}.#{table} JSON '#{escaped}';\n"

        output_file.write(statement)
    end
end

output_file.close

puts "done"