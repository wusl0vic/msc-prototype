package com.dynatrace.msc.dore.livy.jobs.top;

import com.dynatrace.msc.dore.algorithm.CountMinSketchX;
import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.CounterImpl;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.coalesce;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;
import static scala.collection.JavaConverters.asScalaIteratorConverter;

public class TopKCMSJob extends BaseJob<List<Counter<String>>> {

	private final String col;
	private final int k;
	private final int capacity;
	private final double eps;
	private final double confidence;

	public TopKCMSJob(String keyspace, String table, String col, int k, int capacity, double eps, double confidence) {
		this.col = col;
		this.k = k;
		this.capacity = capacity;
		this.eps = eps;
		this.confidence = confidence;
		setOptions(keyspace, table);
	}

	@Override
	public List<Counter<String>> call(JobContext ctx) throws Exception {
		CountMinSketchX cmsX = getDataset(ctx)
				.select(coalesce(asScalaIteratorConverter(Arrays.asList(col(col), lit("null")).iterator()).asScala().toSeq())).as(Encoders.STRING()).javaRDD()
				.mapPartitions(it -> {
					CountMinSketchX cms = new CountMinSketchX(eps, confidence, 42, capacity);
					while (it.hasNext()) {
						cms.add(it.next(), 1);
					}
					return Collections.singleton(cms).iterator();
				}).reduce(CountMinSketchX::mergeX);

		return cmsX.topK(k).entrySet().stream().map(e -> new CounterImpl<>(e.getKey(), e.getValue()))
				.collect(Collectors.toList());
	}
}
