package com.dynatrace.msc.dore.livy.jobs.top;

import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.CounterImpl;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoders;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;

public class TopKSpaceSavingStringJob extends BaseJob<List<Counter<String>>> {

	private final String col;
	private final int k;
	private final int capacity;

	public TopKSpaceSavingStringJob(String keyspace, String table, String col, int k, int capacity) {
		this.col = col;
		this.k = k;
		this.capacity = capacity;
		setOptions(keyspace, table);
	}

	@Override
	public List<Counter<String>> call(JobContext ctx) throws Exception {
		StreamSummary<String> res = getDataset(ctx).select(col(col)).as(Encoders.STRING()).javaRDD()
				.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
		List<StreamSummary<String>.Counter<String>> top = res.topK(k);
		return top.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount(), c.getError()))
				.collect(Collectors.toList());
	}
}
