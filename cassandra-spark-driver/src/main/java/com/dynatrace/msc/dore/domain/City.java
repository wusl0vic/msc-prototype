package com.dynatrace.msc.dore.domain;

import java.io.Serializable;
import java.util.Objects;

public class City implements Serializable {

	private String city;
	private long count;

	public City(String city, long count) {
		this.city = city;
		this.count = count;
	}

	public City() {

	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return city + ": " + count;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		City city1 = (City) o;
		return count == city1.count &&
				Objects.equals(city, city1.city);
	}

	@Override
	public int hashCode() {
		return Objects.hash(city, count);
	}
}
