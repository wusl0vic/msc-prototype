package com.dynatrace.msc.dore.livy.jobs.basic;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;

public class CountJob extends BaseJob<Long> {

	public CountJob(String keyspace, String table) {
		setOptions(keyspace, table);
	}

	@Override
	public Long call(JobContext ctx) throws Exception {
		return getDataset(ctx).count();
	}
}
