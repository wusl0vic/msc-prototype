package com.dynatrace.msc.dore.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StreamSummary<T extends Serializable> implements Serializable {

	private int capacity;
	private Map<T, Counter<T>> counterMap;
	private LinkedList<Bucket> bucketList;

	public StreamSummary(final int capacity) {
		this.capacity = capacity;
		this.counterMap = new HashMap<>();
		this.bucketList = new LinkedList<>();
	}

	public int getCapacity() {
		return capacity;
	}

	public boolean offer(final T item) {
		return offer(item, 1);
	}

	public boolean offer(final T item, final long frequency) {
		Counter<T> counter = counterMap.get(item);
		final boolean newItem = counter == null;
		if (newItem) {
			if (counterMap.size() < capacity) {
				Bucket bucket = new Bucket(0);
				counter = new Counter<>(bucket, item);
				bucket.counters.add(counter);
				bucketList.addLast(bucket);
			} else {
				Bucket min = bucketList.getLast();
				counter = min.counters.get(min.counters.size() - 1);
				counterMap.remove(counter.item);
				counter.item = item;
				counter.error = min.count;
			}
			counterMap.put(item, counter);
		}

		incrementCounter(counter, frequency);

		return newItem;
	}

	public StreamSummary<T> offerAndReturn(final T item) {
		offer(item);
		return this;
	}

	private void incrementCounter(final Counter<T> counter, final long inc) {
		Bucket bucket = counter.parentBucket;
		bucket.counters.remove(counter);
		counter.count += inc;

		int bucketIdx = bucketList.indexOf(bucket);

		boolean foundRightBucket = false;

		for (int i = bucketIdx; i > 0 && i < bucketList.size(); i++) {
			Bucket currentBucket = bucketList.get(i);
			if (counter.count == currentBucket.count) {
				currentBucket.counters.add(counter);
				counter.parentBucket = currentBucket;
				foundRightBucket = true;
				break;
			}
		}

		if (!foundRightBucket) {
			Bucket newBucket = new Bucket(counter.count);
			newBucket.counters.add(counter);

			// TODO: improve insertion
			for (Bucket curr : bucketList) {
				if (counter.count > curr.count) {
					bucketList.add(bucketList.indexOf(curr), newBucket);
					break;
				}
			}

			counter.parentBucket = newBucket;
		}

		if (bucket.counters.isEmpty()) {
			bucketList.remove(bucket);
		}
	}

	public List<Counter<T>> topK(final int k) {
		List<Counter<T>> res = new ArrayList<>(k);

		for (Bucket bucket : bucketList) {
			for (Counter<T> counter : bucket.counters) {
				if (res.size() == k) {
					return res;
				}
				res.add(counter);
			}
		}

		return res;
	}

	// TODO: improve merging (wrong error values i.e.)
	public StreamSummary<T> merge(final StreamSummary<T> other) {
		other.counterMap.forEach((val, counter) -> offer(val, counter.count));
		return this;
	}

	public StreamSummary<T> merge2(final StreamSummary<T> other) {
		List<Counter<T>> ss_1 = new ArrayList<>(this.counterMap.values());
		List<Counter<T>> ss_2 = new ArrayList<>(other.counterMap.values());

		List<Counter<T>> union = new ArrayList<>();
		union.addAll(ss_1); union.addAll(ss_2);

		StreamSummary<T> res = new StreamSummary<>(this.capacity);

		for (Counter<T> counter : union) {
			if (res.counterMap.containsKey(counter.item)) {
				continue;
			}
			long count_1 = 0; long error_1 = 0;
			long count_2 = 0; long error_2 = 0;

			if (this.counterMap.containsKey(counter.item)) {
				Counter<T> ss_1_counter = this.counterMap.get(counter.item); // ss_1.get(ss_1.indexOf(counter));
				count_1 = ss_1_counter.count;
				error_1 = ss_1_counter.error;
			} else {
				count_1 = this.bucketList.getLast().count;
				error_1 = this.bucketList.getLast().count;
			}

			if (other.counterMap.containsKey(counter.item)) {
				Counter<T> ss_2_counter = other.counterMap.get(counter.item);
				count_2 = ss_2_counter.count;
				error_2 = ss_2_counter.error;
			} else {
				count_2 = other.bucketList.getLast().count;
				error_2 = other.bucketList.getLast().count;
			}

			long count = count_1 + count_2;
			long error = error_1 + error_2;

			Counter<T> newCounter = new Counter<>(counter.item, count, error);
			res.counterMap.put(newCounter.item, newCounter);
		}

		List<Counter<T>> counters = new ArrayList<>(res.counterMap.values());
		counters.sort(Comparator.comparingLong((Counter<T> c) -> c.getCount()).reversed());

		int bucketIdx = 0;
		for (Counter<T> counter : counters) {
			Bucket bucket;
			if (res.bucketList.isEmpty()) {
				res.bucketList.addLast(new Bucket(counter.count));
			}
			bucket = res.bucketList.get(bucketIdx);
			if (bucket.count == counter.count) {
				bucket.counters.add(counter);
				counter.parentBucket = bucket;
			} else {
				bucketIdx++;
				Bucket newBucket = new Bucket(counter.count);
				newBucket.counters.add(counter);
				counter.parentBucket = newBucket;
				res.bucketList.addLast(newBucket);
			}
		}

		return res;
	}

	public class Bucket implements Serializable {
		private List<Counter<T>> counters;
		private long count;

		public Bucket(final long count) {
			this.count = count;
			this.counters = new ArrayList<>();
		}

		public long getCount() {
			return count;
		}
	}

	public class Counter<T2 extends Serializable> implements Serializable, com.dynatrace.msc.dore.query.Counter<T2> {

		private Bucket parentBucket;

		private T2 item;
		private long count;
		private long error;

		public Counter() {

		}

		public Counter(Bucket bucket, T2 item) {
			this.parentBucket = bucket;
			this.item = item;
			this.count = 0;
			this.error = 0;
		}

		Counter(T2 item, long count, long error) {
			this.item = item;
			this.count = count;
			this.error = error;
		}

		@Override
		public T2 getItem() {
			return item;
		}

		@Override
		public long getCount() {
			return count;
		}

		@Override
		public long getError() {
			return error;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder();
			sb.append(item).append(": ").append(count).append(" (error: ").append(error).append(")");
			return sb.toString();
		}
	}

}
