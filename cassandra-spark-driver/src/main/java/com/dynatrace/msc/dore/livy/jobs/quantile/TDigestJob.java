package com.dynatrace.msc.dore.livy.jobs.quantile;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import com.tdunning.math.stats.TDigest;
import org.apache.livy.JobContext;

import static org.apache.spark.sql.functions.col;

public class TDigestJob extends BaseJob<double[]> {

	private final String col;
	private final double[] probabilities;
	private final double compression;

	public TDigestJob(String keyspace, String table, String col, double[] probabilities, double compression) {
		this.col = col;
		this.probabilities = probabilities;
		this.compression = compression;
		setOptions(keyspace, table);
	}

	@Override
	public double[] call(JobContext ctx) throws Exception {
		TDigest td = getDataset(ctx).select(col(col)).javaRDD().aggregate(TDigest.createDigest(compression), (digest, row) -> {
			Object rawValue = row.get(0);
			digest.add(Double.valueOf(rawValue.toString()));
			return digest;
		}, (d1, d2) -> {
			d1.add(d2);
			return d1;
		});

		double[] percentiles = new double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++) {
			percentiles[i] = td.quantile(probabilities[i]);
		}
		return percentiles;
	}
}
