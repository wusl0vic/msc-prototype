package com.dynatrace.msc.dore.main;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapColumnTo;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapRowTo;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.someColumns;
import static com.dynatrace.msc.dore.util.TimeUtils.showAndPrintTime;
import static java.util.Collections.unmodifiableMap;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.sum;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.HdrHistogram.Histogram;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.sketch.CountMinSketch;

import com.datastax.spark.connector.japi.CassandraRow;
import com.datastax.spark.connector.japi.rdd.CassandraTableScanJavaRDD;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.domain.City;
import com.dynatrace.msc.dore.domain.RealEstate;
import com.dynatrace.msc.dore.query.QueryExecutor;
import com.tdunning.math.stats.TDigest;

public class Application {

	private static final String SPARK_MASTER = "spark://localhost:7077";

	private static final String SPARK_CASSANDRA_FORMAT = "org.apache.spark.sql.cassandra";
	private static final String SPARK_CASSANDRA_JAR = "/home/dore/dynatrace/msc/prototype/spark-cassandra-image/scripts/spark-cassandra-connector_2.11-2.3.1.jar";

	private static final Map<String, String> CASSANDRA_OPTIONS;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("table", "test_data");
		tmp.put("keyspace", "kp_test");
		CASSANDRA_OPTIONS = unmodifiableMap(tmp);
	}

	/**
	 * we need to pass the driver jar when running the application via IDE
	 * if we use lambda expressions that need
	 * to be deserialised at the worker nodes
	 */
	private static final String DRIVER_JAR = "/home/dore/dynatrace/msc/prototype/cassandra-spark-driver/build/libs/cassandra-spark-driver-1.0-SNAPSHOT.jar";

	private static final String TDIGEST_JAR = "/home/dore/.gradle/caches/modules-2/files-2.1/com.tdunning/t-digest/3.1/451ed219688aed5821a789428fd5e10426d11312/t-digest-3.1.jar";
	private static final String HDR_HISTO_JAR = "/home/dore/.gradle/caches/modules-2/files-2.1/org.hdrhistogram/HdrHistogram/2.1.10/9e1ac84eed220281841b75e72fb9de5a297fbf04/HdrHistogram-2.1.10.jar";
	private static final String JSR_TWITTER_JAR = "/home/dore/.gradle/caches/modules-2/files-2.1/com.twitter/jsr166e/1.1.0/233098147123ee5ddcd39ffc57ff648be4b7e5b2/jsr166e-1.1.0.jar";

	public static void main(String[] args) {
		final SparkConf cassandraSparkConf = new SparkConf()
				.setJars(new String[] { SPARK_CASSANDRA_JAR, DRIVER_JAR, TDIGEST_JAR, HDR_HISTO_JAR, JSR_TWITTER_JAR })
				.set("spark.kryo.registrator", "com.dynatrace.msc.dore.main.Registrator");

		SparkSession.Builder builder = SparkSession.builder()
				.master(SPARK_MASTER)
				.config("spark.cassandra.connection.host", "localhost")
				.config("spark.pushdown", "true")
				.config("spark.eventLog.enabled", "true")
				.config("spark.eventLog.dir", "file:/tmp/spark-events")
				.config(cassandraSparkConf);

		SparkConf allConf = cassandraSparkConf
				.set("spark.cassandra.connection.host", "localhost")
				.set("spark.pushdown", "true")
				.set("spark.eventLog.enabled", "true")
				.set("spark.eventLog.dir", "file:/tmp/spark-events");


//		testTopK(builder);
//		testCountMinSketch(builder);
//		testPercentiles(builder);
//		testHdrHistogram(builder);
//		testStreamSummary(builder);

//		testStreamSummaryImpl();
//		testStreamSummaryMerge();

//		testQueryExecutor(cassandraSparkConf);
//		testUdf(cassandraSparkConf);

//		testMapPartitions(builder);
//		testSparkCassandraConnector(cassandraSparkConf);

//		testTopKWrite(cassandraSparkConf);
//		testCMS(builder);

//		testPredicatePushdown(allConf);
//		testPredicatePushDownSql(builder);

//		testApproxStats(builder);
		testTopKComp();
	}

	private static void testTopKComp() {
		QueryExecutor executor = QueryExecutor.builder().build();

		List<StreamSummary<String>.Counter<String>> topKSS = executor.topK("city", 4, 50, Encoders.STRING());
		List<QueryExecutor.Counter<String>> topKSpark = executor.topKSparkSQL("city", 4);
		topKSS.forEach(System.out::println);

		System.out.println("=====");
		topKSpark.forEach(System.out::println);
	}

	private static void testApproxStats(SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Percentiles Test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();

		double[] quantiles = data.stat().approxQuantile("latitude", new double[] { 0.1, 0.5, 0.99 }, 0.01);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", quantiles[0], quantiles[1], quantiles[2]);

		long distinctCities = data.select(col("city")).toJavaRDD().countApproxDistinct(0.01);
		System.out.println("distinct cities: " + distinctCities);
	}

	private static void testPredicatePushDownSql(SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Predicate Pushdown Spark SQL").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

		Dataset<RealEstate> citiesDs = ctx.sql("SELECT * FROM data WHERE latitude > cast(38.4 as float)").as(Encoders.bean(RealEstate.class));

		citiesDs.explain(true);

		List<RealEstate> cities = citiesDs.collectAsList();
		for (RealEstate estate : cities) {
			System.out.println(estate.getLatitude());
		}

		Dataset<RealEstate> citiesDsNonCast = ctx.sql("SELECT * FROM data WHERE latitude > 38.4").as(Encoders.bean(RealEstate.class));
		citiesDsNonCast.collectAsList();
	}

	private static void testPredicatePushdown(SparkConf conf) {
		try (JavaSparkContext sc = new JavaSparkContext(SPARK_MASTER, "Predicate Pushdown Test", conf)) {
			CassandraTableScanJavaRDD<RealEstate> rdd = javaFunctions(sc).cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class));
			JavaRDD<RealEstate> filteredRdd = rdd.filter(estate -> estate.getLatitude() > 38.5);

			List<RealEstate> filtered = filteredRdd.collect();
			for (RealEstate realEstate : filtered) {
				System.out.println(realEstate.getStreet() + " " + realEstate.getLatitude());
			}
		}
	}

	private static void testTopKWrite(SparkConf conf) {
		try (JavaSparkContext sc = new JavaSparkContext(SPARK_MASTER, "Read Top Cities and write back", conf)) {
			JavaRDD<String> cityRdd = javaFunctions(sc).cassandraTable("kp_test", "test_data")
					.select("city") // projection is done on the cassandra node
					.map(row -> row.getString("city"));

			// calculate a StreamSummary for each partition
			// -> the calculations should be done locally on the nodes
			// TODO: check data locality
			StreamSummary<String> summary = cityRdd.mapPartitions(it -> {
				StreamSummary<String> ss = new StreamSummary<>(20);
				while (it.hasNext()) {
					ss.offer(it.next());
				}
				return Collections.singleton(ss).iterator();
			}).reduce(StreamSummary::merge);

			List<City> topCities = summary.topK(4)
					.stream()
					.map(c -> new City(c.getItem(), c.getCount()))
					.sorted(Comparator.comparingLong(City::getCount).reversed())
					.collect(Collectors.toList());

			topCities.forEach(System.out::println);

			JavaRDD<City> topCityRdd = sc.parallelize(topCities);
			javaFunctions(topCityRdd).writerBuilder("kp_test", "top_cities", mapToRow(City.class)).saveToCassandra();
		}
	}


	private static void testMapPartitions(SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Map Partitions test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

		JavaRDD<String> cityRdd = data.select(col("city")).as(Encoders.STRING()).javaRDD();

		StringBuilder res = cityRdd.mapPartitionsWithIndex((i, it) -> {
			StringBuilder sb = new StringBuilder();
			sb.append("Partition ").append(i);
			int cnt = 0;
			while (it.hasNext()) {
				cnt++;
				it.next();
			}
			sb.append(": ").append(cnt).append(" cities\n");
			return Collections.singleton(sb).iterator();
		}, false).reduce((sb1, sb2) -> {
			return new StringBuilder().append(sb1).append('\n').append(sb2);
		});

		System.out.println(res.toString());
	}

	private static void testSparkCassandraConnector(SparkConf conf) {
		try (JavaSparkContext sc = new JavaSparkContext(SPARK_MASTER, "test app", conf)) {
			CassandraTableScanJavaRDD<CassandraRow> cassandraRdd = javaFunctions(sc)
					.cassandraTable("kp_test", "test_data").select("city");

			long count = cassandraRdd.countApproxDistinct(0.5);
			System.out.println(count);

//			javaFunctions(cassandraRdd).repartitionByCassandraReplica("kp_test", "test_data", )
			StreamSummary<String> summary = cassandraRdd.mapPartitions(it -> {
				StreamSummary<String> ss = new StreamSummary<>(20);
				while (it.hasNext()) {
					ss.offer(it.next().getString(0));
				}
				return Collections.singleton(ss).iterator();
			}).reduce(StreamSummary::merge);

			summary.topK(4).forEach(System.out::println);

//			// TODO: get repartitioning to work
//			JavaRDD<CassandraRow> repartitionedRdd = javaFunctions(cassandraRdd).repartitionByCassandraReplica(
//					"kp_test",
//					"test_data",
//					10,
//					someColumns("city"),
//					mapToRow(CassandraRow.class, Pair.of("city", "city")));
//
//			repartitionedRdd.foreach(System.out::println);

			CassandraTableScanJavaRDD<CassandraRow> table = javaFunctions(sc).cassandraTable("kp_test", "test_data");
//			System.out.println(table.mapToDouble(row -> row.getInt("beds")).stats());


			CassandraTableScanJavaRDD<City> cityRdd = javaFunctions(sc).cassandraTable("kp_test", "test_data", mapRowTo(City.class)).select("city");
			StringBuilder res = cityRdd.mapPartitionsWithIndex((i, it) -> {
				StringBuilder sb = new StringBuilder();
				sb.append("Partition ").append(i).append(": ");
				int c = 0;
				while (it.hasNext()) {
					c++;
					it.next();
				}
				sb.append(c).append(" cities\n");
				return Collections.singleton(sb).iterator();
			}, false).reduce((sb1, sb2) -> {
				sb1.append(sb2).append('\n');
				return sb1;
			});

			System.out.println(res.toString());

			JavaRDD<City> repartitionedRdd = javaFunctions(cityRdd).repartitionByCassandraReplica(
					"kp_test",
					"test_data",
					10,
					someColumns("city"),
					mapToRow(City.class, Pair.of("city", "city")));

			res = repartitionedRdd.mapPartitionsWithIndex((i, it) -> {
				StringBuilder sb = new StringBuilder();
				sb.append("Partition ").append(i);
				int c = 0;
				while (it.hasNext()) {
					c++;
					it.next();
				}
				sb.append(c).append(" cities\n");
				return Collections.singleton(sb).iterator();
			}, false).reduce((sb1, sb2) -> {
				sb1.append(sb2).append('\n');
				return sb1;
			});

			System.out.println(res.toString());
		}
	}

	private static void testTopK(final SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Top K test").getOrCreate());
		System.out.println("==== Top K test ====");
		System.out.println();
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

		Dataset<Row> citySql = ctx.sql("SELECT city, COUNT(*) as cnt FROM data GROUP BY city ORDER BY cnt DESC LIMIT 3");
//		citySql.explain(true);
		showAndPrintTime(citySql);

		Dataset<Row> freq = data.stat().freqItems(new String[] { "city" }, 0.3);
		showAndPrintTime(freq, false);
	}

	private static void testCountMinSketch(final SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Count Min Sketch test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		CountMinSketch cms = data.stat().countMinSketch(col("city"), 0.1, 0.9, 42);
		long estCnt = cms.estimateCount("SACRAMENTO");
		System.out.println("estimated count for SACRAMENTO: " + estCnt);
	}

	/**
	 * TDigest doesn't seem to work with Spark
	 * as some classes are not serializable
	 * @param builder
	 */
	private static void testPercentiles(final SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Percentiles test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();

		double[] percentiles = data.stat().approxQuantile("longitude", new double[] { 0.1, 0.5, 0.99 }, 0.1);
		System.out.printf("Percentiles: 0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		JavaRDD<Float> lonRdd = data.select(col("longitude")).as(Encoders.FLOAT()).javaRDD();

		TDigest res = lonRdd.mapPartitions(it -> {
			TDigest digest = TDigest.createDigest(200);
			while (it.hasNext()) {
				digest.add(it.next());
			}
			return Collections.singletonList(digest).iterator();
		}).reduce((d1, d2) -> {
			d1.add(d2);
			return d1;
		});

		System.out.printf("Percentiles (TDigest): 0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", res.quantile(0.1), res.quantile(0.5), res.quantile(0.99));
	}

	private static void testHdrHistogram(final SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("HdrHistogram test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();

		JavaRDD<Long> lonRdd = data.select(col("price")).as(Encoders.LONG()).javaRDD();

		Histogram res = lonRdd.mapPartitions(it -> {
			Histogram histogram = new Histogram(5);
			while (it.hasNext()) {
				histogram.recordValue(it.next());
			}
			return Collections.singletonList(histogram).iterator();
		}).reduce((h1, h2) -> {
			h1.copyInto(h2);
			return h2;
		});

		double[] median = data.stat().approxQuantile("price", new double[] { 0.5 }, 0.01);
		data.select(avg(col("price"))).show();
//		data.select(mean(col("price"))).show(); // mean also calculates avg

		long priceSum = data.select(sum("price")).as(Encoders.LONG()).toJavaRDD().first();
		System.out.println("sum of price is " + priceSum);

		System.out.println("Median of price: " + res.getValueAtPercentile(0.5));
		System.out.println("Median built in: " + median[0]);
		System.out.println();
	}

	private static void testStreamSummary(final SparkSession.Builder builder) {
		SQLContext ctx = new SQLContext(builder.appName("Stream Summary Stream lib test").getOrCreate());
		Dataset<Row> data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

		JavaRDD<String> cityRdd = data.select(col("city")).as(Encoders.STRING()).javaRDD();

		StreamSummary<String> streamSummary = cityRdd.mapPartitionsWithIndex((i, it) -> {
			System.out.println("Mapping partition " + i);
			StreamSummary<String> ss = new StreamSummary<>(50);
			while (it.hasNext()) {
				ss.offer(it.next());
			}
			return Collections.singletonList(ss).iterator();
		}, false).reduce(StreamSummary::merge);

		List<StreamSummary<String>.Counter<String>> counters = streamSummary.topK(4);
		counters.forEach(System.out::println);
		System.out.println();

		ctx.sql("SELECT city, COUNT(*) as cnt FROM data GROUP BY city ORDER BY cnt DESC LIMIT 4").show();
	}

	private static void testStreamSummaryImpl() {
		com.dynatrace.msc.dore.algorithm.StreamSummary<String> ss = new com.dynatrace.msc.dore.algorithm.StreamSummary<>(6);
		String[] stream = { "AT", "AT", "DE", "AT", "JP", "DE", "AT", "JP", "AUS", "IRE", "JP", "USA", "AT", "JP" };
		for (String s : stream) {
			ss.offer(s);
		}

		List<com.dynatrace.msc.dore.algorithm.StreamSummary<String>.Counter<String>> res = ss.topK(3);
		res.forEach(System.out::println);
	}

	private static void testStreamSummaryMerge() {
		StreamSummary<String> ss1 = new StreamSummary<>(6);
		StreamSummary<String> ss2 = new StreamSummary<>(6);
		String[] stream1 = { "AT", "AT", "DE", "AT", "JP", "DE", "AT", "JP", "AUS", "IRE", "JP", "USA", "AT", "JP" };
		String[] stream2 = { "DE", "AT", "JP", "JP", "AT", "DE", "AUS" };
		for (String s : stream1) {
			ss1.offer(s);
		}
		for (String s : stream2) {
			ss2.offer(s);
		}

		StopWatch timer = new StopWatch();
		timer.start();
		StreamSummary<String> merged = ss1.merge(ss2); timer.stop(); long timeMerge = timer.getTime(); timer.reset();

		timer.start();
		StreamSummary<String> merged2 = ss1.merge2(ss2); timer.stop(); long timeMerge2 = timer.getTime(); timer.reset();

		merged.topK(4).forEach(System.out::println);
		System.out.println("took " + timeMerge + " ms");
		System.out.println();
		merged2.topK(4).forEach(System.out::println);
		System.out.println("took " + timeMerge2 + " ms");
	}

	private static void testQueryExecutor(final SparkConf conf) {
		QueryExecutor executor = QueryExecutor.builder().sparkConf(conf).options(CASSANDRA_OPTIONS).build();
		long sum = executor.sum("price");
		System.out.println("sum: " + sum);

		//executor.topKSpaceSaving("city", 4, 100).forEach(System.out::println);
		double[] percentiles = executor.percentilesGreenwaldKhanna("price", new double[] { 0.0, 0.5, 1.0 }, 0.01);
		System.out.printf("Percentiles: 0.0: %.2f, 0.5: %.2f, 1.0: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		System.out.println("min: " + executor.min("price"));
		System.out.println("max: " + executor.max("price"));
		System.out.println("avg: " + executor.avg("price"));

		Dataset<Row> sqlData = executor.querySql("SELECT city, COUNT(*) as cnt FROM data GROUP BY city ORDER BY cnt DESC LIMIT 4", "data");
		showAndPrintTime(sqlData);
	}

	private static void testUdf(final SparkConf conf) {

	}

}
