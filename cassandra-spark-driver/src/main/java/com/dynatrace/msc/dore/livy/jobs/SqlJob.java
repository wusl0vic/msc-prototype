package com.dynatrace.msc.dore.livy.jobs;

import org.apache.livy.JobContext;

import java.util.Map;

public class SqlJob extends BaseJob<Object> {

	private final String sql;

	public SqlJob(String sql, Map<String, String> options) {
		super(options);
		this.sql = sql;
	}

	@Override
	public Object call(JobContext ctx) throws Exception {
		return getDataset(ctx).sqlContext().sql(sql).collect();
	}
}
