package com.dynatrace.msc.dore.spring.controllers.dto;

public enum PercentileAlgorithm {
	TD, HDR, GK
}
