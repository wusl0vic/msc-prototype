package com.dynatrace.msc.dore.spring.controllers;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.SparkExecutor;
import com.dynatrace.msc.dore.spring.controllers.dto.PercentileRequest;
import com.dynatrace.msc.dore.spring.controllers.dto.TopKRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.Path;

@RestController
@RequestMapping("/livy")
@Api(value = "livy-controller", description = "Submits jobs to the Livy Server which transforms them to Spark Jobs on the cluster")
public class LivyController {

	@Autowired
	private SparkExecutor livy;

	@DeleteMapping
	@ApiOperation(value = "Shuts down the livy session")
	public ResponseEntity<?> killSession() {
		try {
			livy.close();
			return ResponseEntity.ok("Livy Session has been shut down");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error shutting down livy session");
		}
	}

	@GetMapping("/median/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> getMedian(@PathVariable String keyspace, @PathVariable String table, @PathVariable String col) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Double> future = livy.median(keyspace, table, col);
		future.thenApply(median -> result.setResult(ResponseEntity.ok(median)))
			.exceptionally(result::setErrorResult);

		return result;
	}

	@PostMapping("/percentiles/{keyspace}/{table}")
	@ApiOperation(value = "calculate the percentiles with the parameters in the request body")
	public DeferredResult<ResponseEntity<?>> percentiles(@PathVariable String keyspace, @PathVariable String table, @RequestBody PercentileRequest req) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<double[]> future;
		switch (req.getAlgorithm()) {
			case TD:
				future = livy.percentilesTD(keyspace, table, req.getColumn(), req.getProbabilities(), req.getCompression());
				break;
			case HDR:
				future = livy.percentilesHDR(keyspace, table, req.getColumn(), req.getProbabilities(), req.getSignificantValueDigits(), req.getHighestToLowestValueRange());
				break;
			case GK:
			default:
				future = livy.percentilesGK(keyspace, table, req.getColumn(), req.getProbabilities(), req.getRelativeError());
				break;
		}

		future.thenApply(p -> result.setResult(ResponseEntity.ok(p)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@PostMapping("/top/{k}/{keyspace}/{table}/{col}")
	@ApiOperation("Calculate the top k elements for the given column. The desired algorithm can be chosen via the request body. If the body is omitted, an exact result is returned.")
	public DeferredResult<ResponseEntity<?>> topKForCol(
			@PathVariable int k,
			@PathVariable String keyspace,
			@PathVariable String table,
			@PathVariable String col,
			@RequestBody(required = false) TopKRequest req) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<List<Counter<String>>> future;
		if (req != null && req.getAlgorithm() != null) {
			// TODO: set appropriate default value for capacity
			int capacity = req.getCapacity() != null ? req.getCapacity() : (int) Math.round(k * 1.5);
			switch (req.getAlgorithm()) {
				case COUNT_MIN_SKETCH:
					future = livy.topKCountMinSketch(keyspace, table, col, k, capacity, req.getEpsilon(), req.getConfidence());
					break;
				case SPACE_SAVING:
				default:
					future = livy.topKSpaceSaving(keyspace, table, col, k, capacity);
			}
		} else {
			future = livy.topKExact(keyspace, table, col, k);
		}

		future.thenApply(top -> result.setResult(ResponseEntity.ok(top)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/cardinality/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> cardinality(
			@PathVariable String keyspace,
			@PathVariable String table,
			@PathVariable String col,
			@RequestParam(defaultValue = "false") boolean exact,
			@RequestParam(defaultValue = "0.0", required = false) double eps) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Long> future = livy.cardinality(keyspace, table, col, eps, exact);
		future.thenApply(c -> result.setResult(ResponseEntity.ok(c)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/max/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> max(@PathVariable String keyspace, @PathVariable String table, @PathVariable String col) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Long> future = livy.max(keyspace, table, col);
		future.thenApply(max -> result.setResult(ResponseEntity.ok(max)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/min/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> min(@PathVariable String keyspace, @PathVariable String table, @PathVariable String col) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Long> future = livy.min(keyspace, table, col);
		future.thenApply(min -> result.setResult(ResponseEntity.ok(min)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/sum/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> sum(@PathVariable String keyspace, @PathVariable String table, @PathVariable String col) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Long> future = livy.sum(keyspace, table, col);
		future.thenApply(sum -> result.setResult(ResponseEntity.ok(sum)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/avg/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> avg(@PathVariable String keyspace, @PathVariable String table, @PathVariable String col) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Double> future = livy.avg(keyspace, table, col);
		future.thenApply(avg -> result.setResult(ResponseEntity.ok(avg)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	@GetMapping("/count/{keyspace}/{table}/{col}")
	public DeferredResult<ResponseEntity<?>> count(@PathVariable String keyspace, @PathVariable String table) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Long> future = livy.count(keyspace, table);
		future.thenApply(count -> result.setResult(ResponseEntity.ok(count)))
				.exceptionally(result::setErrorResult);

		return result;
	}

	// TODO: return typed response (as of now the response object seems to be empty)
	@PostMapping("/sql")
	public DeferredResult<ResponseEntity<?>> sql(@RequestBody String query) {
		DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();

		CompletableFuture<Object> future = livy.sql(query);
		future.thenApply(res -> result.setResult(ResponseEntity.ok(res)))
				.exceptionally(result::setErrorResult);

		return result;
	}

}
