package com.dynatrace.msc.dore.livy.jobs.top;

import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.CounterImpl;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoder;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;

public class TopKSpaceSavingJob<T extends Serializable> extends BaseJob<List<Counter<T>>> {

	private final String col;
	private final int k;
	private final int capacity;
	private final Encoder<T> encoder;

	public TopKSpaceSavingJob(String keyspace, String table, String col, int k, int capacity, Encoder<T> encoder) {
		this.col = col;
		this.k = k;
		this.capacity = capacity;
		this.encoder = encoder;
		setOptions(keyspace, table);
	}

	@Override
	public List<Counter<T>> call(JobContext ctx) throws Exception {
		// TODO: to cache or not to cache?
		StreamSummary<T> res = getDataset(ctx).select(col(col)).as(encoder).javaRDD()
				.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
//		try {
//			res = ctx.getSharedObject("shared-top-" + k + "-" + col + "-cap-" + capacity);
//		} catch (NoSuchElementException e) {
//			res = getDataset(ctx).select(col(col)).as(encoder).javaRDD()
//					.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
//			ctx.setSharedObject("shared-top-" + k + "-" + col + "-cap-" + capacity, res);
//		}

		List<StreamSummary<T>.Counter<T>> list = res.topK(k);
		return list.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount(), c.getError()))
				.collect(Collectors.toList());
	}
}
