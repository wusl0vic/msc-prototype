package com.dynatrace.msc.dore.domain;

import java.io.Serializable;
import java.util.Objects;

public class RealEstate implements Serializable {

	private String street;
	private String city;
	private String zip;
	private String state;
	private int beds;
	private int baths;
	private int sq__ft;
	private String type;
	private String sale_date;
	private int price;
	private float latitude;
	private float longitude;

	public RealEstate() {

	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getBeds() {
		return beds;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}

	public int getBaths() {
		return baths;
	}

	public void setBaths(int baths) {
		this.baths = baths;
	}

	public int getSq__ft() {
		return sq__ft;
	}

	public void setSq__ft(int sq__ft) {
		this.sq__ft = sq__ft;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSale_date() {
		return sale_date;
	}

	public void setSale_date(String sale_date) {
		this.sale_date = sale_date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RealEstate that = (RealEstate) o;
		return beds == that.beds &&
				baths == that.baths &&
				sq__ft == that.sq__ft &&
				price == that.price &&
				Float.compare(that.latitude, latitude) == 0 &&
				Float.compare(that.longitude, longitude) == 0 &&
				Objects.equals(street, that.street) &&
				Objects.equals(city, that.city) &&
				Objects.equals(zip, that.zip) &&
				Objects.equals(state, that.state) &&
				Objects.equals(type, that.type) &&
				Objects.equals(sale_date, that.sale_date);
	}

	@Override
	public int hashCode() {
		return Objects.hash(street, city, zip, state, beds, baths, sq__ft, type, sale_date, price, latitude, longitude);
	}

	@Override
	public String toString() {
		return "RealEstate{" +
				"street='" + street + '\'' +
				", city='" + city + '\'' +
				'}';
	}
}
