package com.dynatrace.msc.dore.util;

import java.util.concurrent.Callable;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.spark.sql.Dataset;

public class TimeUtils {

	private static StopWatch timer = new StopWatch();

	public static void showAndPrintTime(final Dataset<?> ds) {
		showAndPrintTime(ds, true);
	}

	public static void showAndPrintTime(final Dataset<?> ds, final boolean truncate) {
		timer.start();
		ds.show(truncate);
		timer.stop();
		System.out.println("Took " + timer.getTime() + " ms");
		timer.reset();
	}

	public static <T> T measureTime(Callable<T> func) throws Exception {
		timer.start();
		T res = func.call();
		timer.stop();
		System.out.println("Took " + timer.getTime() + " ms");
		timer.reset();
		return res;
	}

}
