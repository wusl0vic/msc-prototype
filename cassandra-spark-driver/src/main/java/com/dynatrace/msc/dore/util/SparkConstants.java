package com.dynatrace.msc.dore.util;

import static java.util.Collections.unmodifiableMap;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.SparkConf;

import com.dynatrace.msc.dore.main.Registrator;

public final class SparkConstants {

	public static final String SPARK_MASTER = "spark://localhost:7077";
	public static final String SPARK_CASSANDRA_FORMAT = "org.apache.spark.sql.cassandra";
	public static final String LIVY_URL = "http://localhost:8998";

//	public static final String PATH_PREFIX = "/home/dore/dynatrace/msc/prototype";
//	public static final String PATH_PREFIX = "/home/dore/msc-prototype";
	public static final String HOME_PREFIX = "/home/dominik";
	public static final String PATH_PREFIX = "/home/dominik/SEM/msc-prototype";

	/**
	 * we need to pass the driver jar when running the application via IDE
	 * if we use lambda expressions that need
	 * to be deserialised at the worker nodes
	 */
	public static final String DRIVER_JAR = PATH_PREFIX + "/cassandra-spark-driver/build/libs/cassandra-spark-driver-1.0-SNAPSHOT.jar";

	public static final String TEST_JAR = PATH_PREFIX + "/cassandra-spark-driver/build/libs/cassandra-spark-driver-1.0-SNAPSHOT-tests.jar";

	public static final String BOOT_JAR = PATH_PREFIX + "/cassandra-spark-driver/build/libs/cs-spring-boot-0.1.0.jar";

	public static final String SPARK_CASSANDRA_JAR = PATH_PREFIX + "/spark-cassandra-image/scripts/spark-cassandra-connector_2.11-2.3.1.jar";
	public static final String TDIGEST_JAR = HOME_PREFIX + "/.gradle/caches/modules-2/files-2.1/com.tdunning/t-digest/3.2/2ab94758b0276a8a26102adf8d528cf6d0567b9a/t-digest-3.2.jar";
	public static final String HDR_HISTO_JAR = HOME_PREFIX + "/.gradle/caches/modules-2/files-2.1/org.hdrhistogram/HdrHistogram/2.1.10/9e1ac84eed220281841b75e72fb9de5a297fbf04/HdrHistogram-2.1.10.jar";
	public static final String JSR_TWITTER_JAR = HOME_PREFIX + "/.gradle/caches/modules-2/files-2.1/com.twitter/jsr166e/1.1.0/233098147123ee5ddcd39ffc57ff648be4b7e5b2/jsr166e-1.1.0.jar";
	public static final String LIVY_JAR = HOME_PREFIX + "/.gradle/caches/modules-2/files-2.1/org.apache.livy/livy-api/0.5.0-incubating/b90292d2c2946df277385da543239d674cd0dcfd/livy-api-0.5.0-incubating.jar";

	private static final String[] jars = new String[] { SPARK_CASSANDRA_JAR, TDIGEST_JAR, HDR_HISTO_JAR, JSR_TWITTER_JAR, DRIVER_JAR };
	private static final String[] testJars = new String[] { SPARK_CASSANDRA_JAR, TDIGEST_JAR, HDR_HISTO_JAR, JSR_TWITTER_JAR, TEST_JAR, DRIVER_JAR };

	public static String[] getJars() {
		return jars;
	}

	public static String[] getTestJars() {
		return testJars;
	}

	public static String[] getBootJars() {
		return (String[]) ArrayUtils.addAll(jars,new String[] {BOOT_JAR, LIVY_JAR});
	}

	public static final Map<String, String> CASSANDRA_OPTIONS_TEST_TABLE;
	public static final Map<String, String> USERSESSION_TABLE_OPTIONS;
	public static final SparkConf SPARK_CONF;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("table", "test_data");
		tmp.put("keyspace", "kp_test");
		CASSANDRA_OPTIONS_TEST_TABLE = unmodifiableMap(tmp);

		tmp.put("table", "usersession");
		USERSESSION_TABLE_OPTIONS = unmodifiableMap(tmp);

		SPARK_CONF = new SparkConf()
				.setMaster(SPARK_MASTER)
				.setJars(getBootJars())
				.set("spark.cassandra.connection.host", "localhost")
				.set("spark.eventLog.enabled", "true")
//				.set("spark.eventLog.dir", "file:/tmp/spark-events")
//				.set("spark.history.fs.logDirectory", "file:/tmp/spark-events")
				.set("spark.eventLog.dir", "file:" + PATH_PREFIX + "/cluster/history")
				.set("spark.history.fs.logDirectory", "file:" + PATH_PREFIX + "/cluster/history")
				.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
				.set("spark.kryo.registrator", Registrator.class.getName())
				// disable dynamic allocation and set props for shuffling etc.
				.set("spark.shuffle.service.enabled", "false")
				.set("spark.dynamicAllocation.enabled", "false")
				.set("spark.io.compression.codec", "snappy")
				.set("spark.rdd.compress", "true")
				// configure executors
				.set("spark.executor.instances", "4")
				.set("spark.executor.cores", "8")
				.set("spark.pushdown", "true");
	}

	private SparkConstants() {

	}

}
