package com.dynatrace.msc.dore.spring.controllers.dto;

import io.swagger.annotations.ApiModelProperty;

public class PercentileRequest {

	@ApiModelProperty(
			notes = "The algorithm used to calculate the percentiles",
			allowableValues = "GK,TD,HDR",
			required = true)
	private PercentileAlgorithm algorithm;

	@ApiModelProperty(
			notes = "The column name for which the percentiles shall be calculated")
	private String column;

	@ApiModelProperty(
			notes = "The probabilities to be calculated, e.g. 0.5 = 50% quantile = median",
			required = true)
	private double[] probabilities;

	@ApiModelProperty(notes = "Compression parameter for TDigest algorithm. Suggested value is 200")
	private Double compression;

	@ApiModelProperty(
			notes = "Parameter for HDR algorithm",
			allowableValues = "range[1,5]")
	private Integer significantValueDigits;

	@ApiModelProperty(notes = "Range of possible values for HDR algorithm")
	private Long highestToLowestValueRange;

	@ApiModelProperty(notes = "accuracy parameter for GK algorithm")
	private Double relativeError;

	public PercentileRequest() {

	}

	public PercentileAlgorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(PercentileAlgorithm algorithm) {
		this.algorithm = algorithm;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public double[] getProbabilities() {
		return probabilities;
	}

	public void setProbabilities(double[] probabilities) {
		this.probabilities = probabilities;
	}

	public Double getCompression() {
		return compression;
	}

	public void setCompression(Double compression) {
		this.compression = compression;
	}

	public Integer getSignificantValueDigits() {
		return significantValueDigits;
	}

	public void setSignificantValueDigits(Integer significantValueDigits) {
		this.significantValueDigits = significantValueDigits;
	}

	public Double getRelativeError() {
		return relativeError;
	}

	public void setRelativeError(Double relativeError) {
		this.relativeError = relativeError;
	}

	public Long getHighestToLowestValueRange() {
		return highestToLowestValueRange;
	}

	public void setHighestToLowestValueRange(Long highestToLowestValueRange) {
		this.highestToLowestValueRange = highestToLowestValueRange;
	}
}
