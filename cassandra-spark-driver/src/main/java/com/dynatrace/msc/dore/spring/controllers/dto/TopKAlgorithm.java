package com.dynatrace.msc.dore.spring.controllers.dto;

public enum TopKAlgorithm {
	SPACE_SAVING, COUNT_MIN_SKETCH
}
