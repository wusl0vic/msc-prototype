package com.dynatrace.msc.dore.livy.jobs.quantile;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;

public class GKJob extends BaseJob<double[]> {

	private final String col;
	private final double[] probabilities;
	private final double relativeError;

	public GKJob(String keyspace, String table, String col, double[] probabilities, double relativeError) {
		this.col = col;
		this.probabilities = probabilities;
		this.relativeError = relativeError;
		setOptions(keyspace, table);
	}

	@Override
	public double[] call(JobContext ctx) throws Exception {
		return getDataset(ctx).stat().approxQuantile(col, probabilities, relativeError);
	}
}
