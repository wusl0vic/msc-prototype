package com.dynatrace.msc.dore.livy.jobs.basic;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.functions;

public class MaxJob extends BaseJob<Long> {

	private final String col;

	public MaxJob(String keyspace, String table, String col) {
		this.col = col;
		setOptions(keyspace, table);
	}

	@Override
	public Long call(JobContext ctx) throws Exception {
		return getDataset(ctx).select(functions.max(col)).as(Encoders.LONG()).first();
	}
}
