package com.dynatrace.msc.dore.livy.jobs.quantile;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.HdrHistogram.DoubleHistogram;
import org.apache.livy.JobContext;

import static org.apache.spark.sql.functions.col;

public class HDRJob extends BaseJob<double[]> {

	private final String col;
	private final double[] probabilities;
	private final int significantValueDigits;
	private final long ratio;

	public HDRJob(String keyspace, String table, String col, double[] probabilities, int significantValueDigits, long highestToLowestValueRatio) {
		this.col = col;
		this.probabilities = probabilities;
		this.significantValueDigits = significantValueDigits;
		this.ratio = highestToLowestValueRatio;
		setOptions(keyspace, table);
	}

	@Override
	public double[] call(JobContext ctx) throws Exception {
		DoubleHistogram histogram = getDataset(ctx).select(col(col)).javaRDD()
			.aggregate(new DoubleHistogram(ratio, significantValueDigits), (hg, row) -> {
				Object rawValue = row.get(0);
				hg.recordValue(Double.valueOf(rawValue.toString()));
				return hg;
			}, (hg1, hg2) -> {
				hg1.add(hg2);
				return hg1;
			});

		double[] percentiles = new double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++) {
			percentiles[i] = histogram.getValueAtPercentile(probabilities[i] * 100);
		}

		return percentiles;
	}
}
