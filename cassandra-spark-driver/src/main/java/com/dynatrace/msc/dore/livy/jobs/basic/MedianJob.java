package com.dynatrace.msc.dore.livy.jobs.basic;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Row;

import java.util.List;

import static org.apache.spark.sql.functions.col;

public class MedianJob extends BaseJob<Double> {

	private String col;

	public MedianJob(String keyspace, String table, String col) {
		this.col = col;
		setOptions(keyspace, table);
	}

	@Override
	public Double call(JobContext ctx) throws Exception {
		List<Row> sorted = getDataset(ctx).filter(String.format("%s is not null", col)).sort(col(col)).select(col(col)).collectAsList();
		if (sorted.size() % 2 != 0) {
			return Double.valueOf(sorted.get(sorted.size() / 2).get(0).toString()) + Double.valueOf(sorted.get(sorted.size() / 2 - 1).get(0).toString()) / 2;
		} else {
			return Double.valueOf(sorted.get(sorted.size() / 2).get(0).toString());
		}
	}
}
