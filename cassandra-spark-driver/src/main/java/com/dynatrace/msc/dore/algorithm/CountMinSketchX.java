package com.dynatrace.msc.dore.algorithm;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.clearspring.analytics.stream.frequency.CountMinSketch;

public class CountMinSketchX extends CountMinSketch implements Serializable {

	private final Map<String, Long> topElements;
	private final int x_depth;
	private final int x_width;
	private final int x_seed;
	private final int capacity;

	public CountMinSketchX(int depth, int width, int seed, int capacity) {
		super(depth, width, seed);
		this.topElements = new HashMap<>();
		this.x_depth = depth;
		this.x_width = width;
		this.x_seed = seed;
		this.capacity = capacity;
	}

	public CountMinSketchX(double epsOfTotalCount, double confidence, int seed, int capacity) {
		super(epsOfTotalCount, confidence, seed);
		this.topElements = new HashMap<>();
		this.x_depth = (int) Math.ceil(-Math.log(1 - confidence) / Math.log(2));
		this.x_width = (int) Math.ceil(2 / epsOfTotalCount);
		this.x_seed = seed;
		this.capacity = capacity;
	}

	@Override
	public void add(String item, long count) {
		super.add(item, count);
		if (topElements.size() < capacity) {
			topElements.put(item, estimateCount(item));
		} else {
			Optional<Map.Entry<String, Long>> entryOpt = topElements.entrySet().stream().min(Map.Entry.comparingByValue());
			entryOpt.ifPresent(entry -> {
				topElements.remove(entry.getKey());
				topElements.put(item, estimateCount(item));
			});
		}
	}

	public Map<String, Long> topK(int k) {
		return topElements.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.limit(k)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));
	}

	public CountMinSketchX mergeX(CountMinSketchX other) {
		CountMinSketchX merged = new CountMinSketchX(x_depth, x_width, x_seed, capacity);
		try {
			merged.topElements.putAll(this.topElements);
			other.topElements.forEach((k,v) -> merged.topElements.merge(k, v, (v1, v2) -> v1 + v2));

			// TODO: correctly merge sketches (unnecessary for top k queries)
			CountMinSketch tmp = CountMinSketch.merge(this, other);
//			merged.hashA = tmp.hashA;
//			merged.table = tmp.table;
		} catch (CMSMergeException e) {
			System.err.println("failure while merging CMS");
		}

		return merged;
	}

}
