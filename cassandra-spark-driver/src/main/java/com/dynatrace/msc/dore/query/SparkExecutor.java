package com.dynatrace.msc.dore.query;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.spark.sql.Encoder;

import com.dynatrace.msc.dore.algorithm.StreamSummary;

public interface SparkExecutor extends AutoCloseable {

	CompletableFuture<Double> median(String keyspace, String table, String col);

	CompletableFuture<double[]> percentilesTD(String keyspace, String table, String col, double[] probabilities, double compression);

	CompletableFuture<double[]> percentilesHDR(String keyspace, String table, String col, double[] probabilities, int significantValueDigits, long highestToLowestValueRatio);

	CompletableFuture<double[]> percentilesGK(String keyspace, String table, String col, double[] probabilities, double relativeError);

	CompletableFuture<List<Counter<String>>> topKExact(String keyspace, String table, String col, int k);

	/**
	 * used for testing
	 * (for some reason, passing an encoder does not work when comparing different Top-K algorithms)
	 */
	CompletableFuture<List<Counter<String>>> topKSpaceSaving(String keyspace, String table, String col, int k, int capacity);

	<T extends Serializable> CompletableFuture<List<Counter<T>>> topKSpaceSaving(String keyspace, String table, String col, int k, int capacity, Encoder<T> encoder);

	CompletableFuture<List<Counter<String>>> topKCountMinSketch(String keyspace, String table, String col, int k, int capacity, double eps, double confidence);

	// TODO: fix serialization issues with StreamSummary.Bucket.Counter (see LivyExecutorTest)
	<T extends Serializable> CompletableFuture<StreamSummary<T>> streamSummary(String keyspace, String table, String col, int capacity, Encoder<T> encoder);

	CompletableFuture<Long> cardinality(String keyspace, String table, String col, double eps, boolean exact);

	CompletableFuture<Long> max(String keyspace, String table, String col);

	CompletableFuture<Long> min(String keyspace, String table, String col);

	CompletableFuture<Long> sum(String keyspace, String table, String col);

	CompletableFuture<Double> avg(String keyspace, String table, String col);

	CompletableFuture<Long> count(String keyspace, String table);

	CompletableFuture<Object> sql(String sql);
}
