package com.dynatrace.msc.dore.query;

import java.io.Serializable;

public interface Counter<T extends Serializable> {

	T getItem();
	long getCount();
	long getError();

}
