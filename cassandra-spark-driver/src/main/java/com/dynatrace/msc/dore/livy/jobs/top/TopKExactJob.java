package com.dynatrace.msc.dore.livy.jobs.top;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.CounterImpl;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.List;
import java.util.stream.Collectors;

public class TopKExactJob extends BaseJob<List<Counter<String>>> {

	private final String col;
	private final int k;

	public TopKExactJob(String keyspace, String table, String col, int k) {
		this.col = col;
		this.k  = k;
		setOptions(keyspace, table);
	}

	@Override
	public List<Counter<String>> call(JobContext ctx) throws Exception {
		Dataset<Row> ds = getDataset(ctx).sqlContext().sql(String.format("SELECT COALESCE(%s, 'null') as col, COUNT(*) as count FROM data GROUP BY col ORDER BY count DESC LIMIT %d", col, k));
		return ds.collectAsList().stream()
				.map(row -> new CounterImpl<>(row.get(0).toString(), row.getLong(1)))
				.collect(Collectors.toList());
	}
}
