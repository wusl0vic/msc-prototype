package com.dynatrace.msc.dore.livy.jobs;

import org.apache.livy.JobContext;

import java.util.Map;

import static org.apache.spark.sql.functions.col;

public class CardinalityJob extends BaseJob<Long>  {

	private final double eps;
	private final String col;
	private final boolean exact;

	public CardinalityJob(String col, double eps, boolean exact, Map<String, String> options) {
		super(options);
		this.col = col;
		this.eps = eps;
		this.exact = exact;
	}

	public CardinalityJob(String keyspace, String table, String col, double eps, boolean exact) {
		this(col, eps, exact, null);
		setOptions(keyspace, table);
	}

	@Override
	public Long call(JobContext ctx) throws Exception {
		return exact
				? getDataset(ctx).select(col(col)).distinct().count()
				: getDataset(ctx).select(col(col)).javaRDD().countApproxDistinct(eps);
	}
}
