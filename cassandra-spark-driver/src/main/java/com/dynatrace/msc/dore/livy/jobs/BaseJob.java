package com.dynatrace.msc.dore.livy.jobs;

import org.apache.livy.Job;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;

public abstract class BaseJob<T> implements Job<T> {

	private static final Map<String, String> TEST_OPTIONS;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("keyspace", "kp_test");
//		tmp.put("table", "test_data");
		tmp.put("table", "usersession");
//		TEST_OPTIONS = unmodifiableMap(tmp);
		TEST_OPTIONS = tmp;
	}

	protected Map<String, String> options;

	public BaseJob() {
		this(new HashMap<>());
	}

	public BaseJob(Map<String, String> options) {
		this.options = options != null ? options : new HashMap<>();
	}

	protected Dataset<Row> getDataset(JobContext ctx) {
		if (options.isEmpty()) {
			options = TEST_OPTIONS;
		}
		Dataset<Row> ds;
		String dsIdentifier = "shared-ds-livy-" + options.get("keyspace") + "." + options.get("table");

		try {
			ds = ctx.getSharedObject(dsIdentifier);
			System.out.println("cache hit: " + dsIdentifier);
		} catch (NoSuchElementException e) {
			System.out.println("cache miss: " + dsIdentifier);
			ds = ctx.sqlctx().read().format(SPARK_CASSANDRA_FORMAT)
					.options(options).load();
			ds.createOrReplaceTempView("data");
			ds.cache();
			ctx.setSharedObject(dsIdentifier, ds);
		}

		return ds;
	}

	protected void setOptions(String keyspace, String table) {
		options.put("keyspace", keyspace);
		options.put("table", table);
	}

}
