package com.dynatrace.msc.dore.livy;

import static com.dynatrace.msc.dore.util.SparkConstants.BOOT_JAR;
import static com.dynatrace.msc.dore.util.SparkConstants.LIVY_URL;
import static com.dynatrace.msc.dore.util.SparkConstants.getTestJars;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.livy.Job;
import org.apache.livy.LivyClient;
import org.apache.livy.LivyClientBuilder;
import org.apache.spark.sql.Encoder;
import org.springframework.stereotype.Component;

import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.livy.jobs.CardinalityJob;
import com.dynatrace.msc.dore.livy.jobs.SqlJob;
import com.dynatrace.msc.dore.livy.jobs.basic.AvgJob;
import com.dynatrace.msc.dore.livy.jobs.basic.CountJob;
import com.dynatrace.msc.dore.livy.jobs.basic.MaxJob;
import com.dynatrace.msc.dore.livy.jobs.basic.MedianJob;
import com.dynatrace.msc.dore.livy.jobs.basic.MinJob;
import com.dynatrace.msc.dore.livy.jobs.basic.SumJob;
import com.dynatrace.msc.dore.livy.jobs.quantile.GKJob;
import com.dynatrace.msc.dore.livy.jobs.quantile.HDRJob;
import com.dynatrace.msc.dore.livy.jobs.quantile.TDigestJob;
import com.dynatrace.msc.dore.livy.jobs.top.StreamSummaryJob;
import com.dynatrace.msc.dore.livy.jobs.top.TopKCMSJob;
import com.dynatrace.msc.dore.livy.jobs.top.TopKExactJob;
import com.dynatrace.msc.dore.livy.jobs.top.TopKSpaceSavingJob;
import com.dynatrace.msc.dore.livy.jobs.top.TopKSpaceSavingStringJob;
import com.dynatrace.msc.dore.main.Registrator;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.SparkExecutor;

@Component
public class LivyExecutor implements AutoCloseable, SparkExecutor {

	private final LivyClient client;

//	@Value("${livy.url}")
//	private String livyUrl;
	private Map<String, String> cassandraOptions;

	public LivyExecutor() throws IOException, URISyntaxException {
		this(new HashMap<>());
	}

	public LivyExecutor(Map<String, String> cassandraOptions) throws IOException, URISyntaxException {
		client = new LivyClientBuilder()
				.setURI(new URI(LIVY_URL))
				.setConf("spark.cassandra.connection.host", "node-1")
				.setConf("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
				.setConf("spark.kryo.registrator", Registrator.class.getName())
				.setConf("spark.pushdown", "true")
//				.setConf("spark.eventLog.enabled", "true")
				.setConf("spark.eventLog.dir", "file://history/tmp/spark-events")
				.setConf("spark.history.fs.logDirectory", "file://history/tmp/spark-events")
				.setConf("spark.shuffle.service.enabled", "false")
				.setConf("spark.dynamicAllocation.enabled", "false")
				.setConf("spark.io.compression.codec", "snappy")
				.setConf("spark.rdd.compress", "true")
				// configure executors
				.setConf("spark.executor.instances", "4")
				.setConf("spark.executor.cores", "8")
				.setConf("spark.pushdown", "true")
				.build();
		this.cassandraOptions = cassandraOptions;
	}

	public static LivyExecutor buildNonSpring() throws IOException, URISyntaxException {
		return buildNonSpring(new HashMap<>());
	}

	public static LivyExecutor buildNonSpring(Map<String, String> cassandraOptions) throws IOException, URISyntaxException {
		LivyExecutor livy = new LivyExecutor(cassandraOptions);
		livy.postConstruct();
		return livy;
	}

	public void setCassandraOptions(Map<String, String> cassandraOptions) {
		this.cassandraOptions = cassandraOptions;
	}

	@PostConstruct
	public void postConstruct() {
		for (String jar : getTestJars()) {
			client.uploadJar(new File(jar));
		}
		client.uploadJar(new File(BOOT_JAR));
	}

	@Override
	public CompletableFuture<Double> median(String keyspace, String table, String col) {
		return toCompletableFuture(client.submit(new MedianJob(keyspace, table, col)));
	}

	@Override
	public CompletableFuture<double[]> percentilesTD(String keyspace, String table, String col, double[] probabilities, double compression) {
		return toCompletableFuture(client.submit(new TDigestJob(keyspace, table, col, probabilities, compression)));
	}

	@Override
	public CompletableFuture<double[]> percentilesHDR(String keyspace, String table, String col, double[] probabilities, int significantValueDigits, long highestToLowestValueRatio) {
		return toCompletableFuture(client.submit(new HDRJob(keyspace, table, col, probabilities, significantValueDigits, highestToLowestValueRatio)));
	}

	@Override
	public CompletableFuture<double[]> percentilesGK(String keyspace, String table, String col, double[] probabilities, double relativeError) {
		return toCompletableFuture(client.submit(new GKJob(keyspace, table, col, probabilities, relativeError)));
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKExact(String keyspace, String table, String col, int k) {
		return toCompletableFuture(client.submit(new TopKExactJob(keyspace, table, col, k)));
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKSpaceSaving(String keyspace, String table, String col, int k, int capacity) {
		return toCompletableFuture(client.submit(new TopKSpaceSavingStringJob(keyspace, table, col, k, capacity)));
	}

	@Override
	public <T extends Serializable> CompletableFuture<List<Counter<T>>> topKSpaceSaving(String keyspace, String table, String col, int k, int capacity, Encoder<T> encoder) {
		return toCompletableFuture(client.submit(new TopKSpaceSavingJob<>(keyspace, table, col, k, capacity, encoder)));
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKCountMinSketch(String keyspace, String table, String col, int k, int capacity, double eps, double confidence) {
		return toCompletableFuture(client.submit(new TopKCMSJob(keyspace, table, col, k, capacity, eps, confidence)));
	}

	// TODO: fix serialization issues with StreamSummary.Bucket.Counter (see LivyExecutorTest)
	@Override
	public <T extends Serializable> CompletableFuture<StreamSummary<T>> streamSummary(String keyspace, String table, String col, int capacity,
			Encoder<T> encoder) {
		return toCompletableFuture(client.submit(new StreamSummaryJob<>(keyspace, table, col, capacity, encoder)));
	}

	@Override
	public CompletableFuture<Long> cardinality(String keyspace, String table, String col, double eps, boolean exact) {
		return toCompletableFuture(client.submit(new CardinalityJob(keyspace, table, col, eps, exact)));
	}

	@Override
	public CompletableFuture<Long> max(String keyspace, String table, String col) {
		return toCompletableFuture(client.submit(new MaxJob(keyspace, table, col)));
	}

	@Override
	public CompletableFuture<Long> min(String keyspace, String table, String col) {
		return toCompletableFuture(client.submit(new MinJob(keyspace, table, col)));
	}

	@Override
	public CompletableFuture<Long> sum(String keyspace, String table, String col) {
		return toCompletableFuture(client.submit(new SumJob(keyspace, table, col)));
	}

	@Override
	public CompletableFuture<Double> avg(String keyspace, String table, String col) {
		return toCompletableFuture(client.submit(new AvgJob(keyspace, table, col)));
	}

	@Override
	public CompletableFuture<Long> count(String keyspace, String table) {
		return toCompletableFuture(client.submit(new CountJob(keyspace, table)));
	}

	@Override
	public CompletableFuture<Object> sql(String sql) {
		return toCompletableFuture(client.submit(new SqlJob(sql, cassandraOptions)));
	}

	public <T> CompletableFuture<T> submitJob(Job<T> job) {
		return toCompletableFuture(client.submit(job));
	}

	private static <T> CompletableFuture<T> toCompletableFuture(Future<T> future) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				return future.get();
			} catch (InterruptedException | ExecutionException e) {
				System.err.println(e);
				throw new RuntimeException(e);
			}
		});
	}

	public void stop(boolean shutdownContext) {
		client.stop(shutdownContext);
	}

	@Override
	public void close() throws Exception {
		client.stop(true);
	}
}
