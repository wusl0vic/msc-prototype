package com.dynatrace.msc.dore.query;

import com.dynatrace.msc.dore.algorithm.CountMinSketchX;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.tdunning.math.stats.TDigest;
import org.HdrHistogram.DoubleHistogram;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.storage.StorageLevel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CONF;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_MASTER;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.coalesce;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;
import static scala.collection.JavaConverters.asScalaIteratorConverter;

public class DirectExecutor implements SparkExecutor {

	private Map<String, String> options = new HashMap<>();
	private static Map<String, Dataset<Row>> dsCache = new ConcurrentHashMap<>();

	private SparkSession sparkSession;

	public DirectExecutor(String masterUrl, SparkConf conf) {
		sparkSession= SparkSession.builder()
				.master(masterUrl)
				.config(conf)
				.getOrCreate();
	}

	public DirectExecutor() {
		this(SPARK_MASTER, SPARK_CONF);
	}

	private Dataset<Row> getDs(String keyspace, String table) {
		String dsIdent = "shared-ds-direct-" + keyspace + "." + table;
		if (dsCache.containsKey(dsIdent)) {
			return dsCache.get(dsIdent);
		}
		setOptions(keyspace, table);
		Dataset<Row> ds = sparkSession.read().format(SPARK_CASSANDRA_FORMAT)
				.options(options).load();
		ds.createOrReplaceTempView("data");
		ds.persist(StorageLevel.MEMORY_AND_DISK());
		dsCache.put(dsIdent, ds);
		return ds;
	}

	private void setOptions(String keyspace, String table) {
		options.put("keyspace", keyspace);
		options.put("table", table);
	}

	@Override
	public CompletableFuture<Double> median(String keyspace, String table, String col) {
		return CompletableFuture.supplyAsync(() -> {
			List<Row> sorted = getDs(keyspace, table).filter(col(col).isNotNull()).select(col(col)).sort(asc(col)).collectAsList();
			if (sorted.size() % 2 != 0) {
				return (Double.valueOf(sorted.get(sorted.size() / 2).get(0).toString()) + Double.valueOf(sorted.get(sorted.size() / 2 - 1).get(0).toString())) / 2;
			} else {
				return Double.valueOf(sorted.get(sorted.size() / 2).get(0).toString());
			}
		});
	}

	@Override
	public CompletableFuture<double[]> percentilesTD(String keyspace, String table, String col, double[] probabilities, double compression) {
		return CompletableFuture.supplyAsync(() -> {
			TDigest td = getDs(keyspace, table).select(col(col)).javaRDD().aggregate(TDigest.createDigest(compression), (digest, row) -> {
				Object rawValue = row.get(0);
				digest.add(Double.valueOf(rawValue.toString()));
				return digest;
			}, (d1, d2) -> {
				d1.add(d2);
				return d1;
			});

			double[] percentiles = new double[probabilities.length];
			for (int i = 0; i < probabilities.length; i++) {
				percentiles[i] = td.quantile(probabilities[i]);
			}
			return percentiles;
		});
	}

	@Override
	public CompletableFuture<double[]> percentilesHDR(String keyspace, String table, String col, double[] probabilities,
			int significantValueDigits, long highestToLowestValueRatio) {

		return CompletableFuture.supplyAsync(() -> {
			DoubleHistogram histogram = getDs(keyspace, table).select(col(col)).javaRDD()
					.aggregate(new DoubleHistogram(highestToLowestValueRatio, significantValueDigits), (hg, row) -> {
						Object rawValue = row.get(0);
						hg.recordValue(Double.valueOf(rawValue.toString()));
						return hg;
					}, (hg1, hg2) -> {
						hg1.add(hg2);
						return hg1;
					});

			double[] percentiles = new double[probabilities.length];
			for (int i = 0; i < probabilities.length; i++) {
				percentiles[i] = histogram.getValueAtPercentile(probabilities[i] * 100);
			}

			return percentiles;
		});
	}

	@Override
	public CompletableFuture<double[]> percentilesGK(String keyspace, String table, String col, double[] probabilities,
			double relativeError) {

		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).stat().approxQuantile(col, probabilities, relativeError));
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKExact(String keyspace, String table, String col, int k) {
		return CompletableFuture.supplyAsync(() -> {
			Dataset<Row> ds = getDs(keyspace, table).sqlContext()
					.sql(String.format("SELECT COALESCE(%s, 'null') as col, COUNT(*) as count FROM data GROUP BY col ORDER BY count DESC LIMIT %d", col, k));
			return ds.collectAsList().stream()
					.map(row -> new CounterImpl<>(row.get(0).toString(), row.getLong(1)))
					.collect(Collectors.toList());
		});
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKSpaceSaving(String keyspace, String table, String col, int k, int capacity) {
		return CompletableFuture.supplyAsync(() -> {
			StreamSummary<String> res = getDs(keyspace, table).select(col(col)).as(Encoders.STRING()).javaRDD()
					.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
			List<StreamSummary<String>.Counter<String>> top = res.topK(k);
			return top.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount(), c.getError()))
					.collect(Collectors.toList());
		});
	}

	@Override
	public <T extends Serializable> CompletableFuture<List<Counter<T>>> topKSpaceSaving(String keyspace, String table, String col,
			int k, int capacity, Encoder<T> encoder) {
		return CompletableFuture.supplyAsync(() -> {
			StreamSummary<T> res = getDs(keyspace, table).select(col(col)).as(encoder).javaRDD()
					.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
			List<StreamSummary<T>.Counter<T>> top = res.topK(k);
			return top.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount(), c.getError()))
					.collect(Collectors.toList());
		});
	}

	@Override
	public CompletableFuture<List<Counter<String>>> topKCountMinSketch(String keyspace, String table, String col, int k,
			int capacity, double eps, double confidence) {
		return CompletableFuture.supplyAsync(() -> {
			CountMinSketchX cmsX = getDs(keyspace, table)
					.select(coalesce(asScalaIteratorConverter(Arrays.asList(col(col), lit("null")).iterator()).asScala().toSeq())).as(Encoders.STRING()).javaRDD()
					.mapPartitions(it -> {
						CountMinSketchX cms = new CountMinSketchX(eps, confidence, 42, capacity);
						while (it.hasNext()) {
							cms.add(it.next(), 1);
						}
						return Collections.singleton(cms).iterator();
					}).reduce(CountMinSketchX::mergeX);

			return cmsX.topK(k).entrySet().stream().map(e -> new CounterImpl<>(e.getKey(), e.getValue()))
					.collect(Collectors.toList());
		});
	}

	@Override
	public <T extends Serializable> CompletableFuture<StreamSummary<T>> streamSummary(String keyspace, String table, String col,
			int capacity, Encoder<T> encoder) {
		return null;
	}

	@Override
	public CompletableFuture<Long> cardinality(String keyspace, String table, String col, double eps, boolean exact) {
		return CompletableFuture.supplyAsync(() -> exact
					? getDs(keyspace, table).select(col(col)).distinct().count()
					: getDs(keyspace, table).select(col(col)).javaRDD().countApproxDistinct(eps));
	}

	@Override
	public CompletableFuture<Long> max(String keyspace, String table, String col) {
		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).select(functions.max(col)).as(Encoders.LONG()).first());
	}

	@Override
	public CompletableFuture<Long> min(String keyspace, String table, String col) {
		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).select(functions.min(col)).as(Encoders.LONG()).first());
	}

	@Override
	public CompletableFuture<Long> sum(String keyspace, String table, String col) {
		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).select(functions.sum(col)).as(Encoders.LONG()).first());
	}

	@Override
	public CompletableFuture<Double> avg(String keyspace, String table, String col) {
		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).select(functions.avg(col)).as(Encoders.DOUBLE()).first());
	}

	@Override
	public CompletableFuture<Long> count(String keyspace, String table) {
		return CompletableFuture.supplyAsync(() -> getDs(keyspace, table).count());
	}

	@Override
	public CompletableFuture<Object> sql(String sql) {
		return CompletableFuture.supplyAsync(() -> sparkSession.sql(sql).collect());
	}

	@Override
	public void close() {
		sparkSession.stop();
	}
}
