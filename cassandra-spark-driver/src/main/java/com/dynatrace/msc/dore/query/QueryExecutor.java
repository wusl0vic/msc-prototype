package com.dynatrace.msc.dore.query;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_MASTER;
import static com.dynatrace.msc.dore.util.SparkConstants.getJars;
import static org.apache.spark.sql.functions.col;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.annotation.Nonnegative;

import org.HdrHistogram.DoubleHistogram;
import org.HdrHistogram.Histogram;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import com.dynatrace.msc.dore.algorithm.CountMinSketchX;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.tdunning.math.stats.TDigest;

public class QueryExecutor implements QueryFunctions, Serializable {

	private static final long serialVersionUID = 1L;

	private static SQLContext sqlContext;
	private final Dataset<Row> data;

	private Map<String, String> options;

	public static QueryExecutorBuilder builder() {
		return new QueryExecutorBuilder();
	}

	private QueryExecutor(final SparkConf conf, final Map<String, String> cassandraOptions, final String masterUrl) throws Exception {
		if (sqlContext == null) {
			sqlContext = new SQLContext(SparkSession.builder()
					.master(masterUrl)
					.config(conf)
					.getOrCreate());
		}

		this.options = cassandraOptions;

		this.data = sqlContext.read().format(SPARK_CASSANDRA_FORMAT).options(cassandraOptions).load();
		data.createOrReplaceTempView("data");
	}

	@Override
	public long sum(String col) {
		return data.select(functions.sum(col)).as(Encoders.LONG()).first();
	}

	@Override
	public long min(String col) {
		return data.select(functions.min(col)).as(Encoders.LONG()).first();
	}

	@Override
	public long max(String col) {
		return data.select(functions.max(col)).as(Encoders.LONG()).first();
	}

	@Override
	public double avg(String col) {
		return data.select(functions.avg(col)).as(Encoders.DOUBLE()).first();
	}

	@Override
	public <T extends Serializable> List<StreamSummary<T>.Counter<T>> topK(
			String col, int k, int capacity, Encoder<T> encoder) {

		JavaRDD<T> rdd = data.select(col(col)).as(encoder).javaRDD();
		StreamSummary<T> result = rdd.mapPartitions(it -> {
			StreamSummary<T> summary = new StreamSummary<>(capacity);
			while (it.hasNext()) {
				summary.offer(it.next());
			}
			return Collections.singleton(summary).iterator();
		}).reduce(StreamSummary::merge);

		return result.topK(k);
	}

	public <T extends Serializable> CompletableFuture<List<StreamSummary<T>.Counter<T>>> topKAsync(
			String col, int k, int capacity, Encoder<T> encoder) {
		return CompletableFuture.supplyAsync(() -> topK(col, k, capacity, encoder));
	}

	/**
	 * calculate the top k elements using aggregate instead of mapPartitions
	 * @param col
	 * @param k
	 * @param capacity
	 * @param encoder
	 * @param <T>
	 * @return
	 */
	public <T extends Serializable> List<StreamSummary<T>.Counter<T>> topKAggregate(String col, int k, int capacity, Encoder<T> encoder) {
		JavaRDD<T> rdd = data.select(col(col)).as(encoder).javaRDD();
		StreamSummary<T> result = rdd.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
		return result.topK(k);
	}

//	public <T extends Serializable> List<StreamSummary<T>.Counter<T>> topKAggregate(String col, int k, int capacity, Encoder<T> encoder, JobContext jobContext) {
//		Map<String, String> TEST_OPTIONS = new HashMap<>();
//		TEST_OPTIONS.put("keyspace", "kp_test");
//		TEST_OPTIONS.put("table", "test_data");
//		Dataset<Row> ds = jobContext.sqlctx().read().format(SPARK_CASSANDRA_FORMAT).TEST_OPTIONS(TEST_OPTIONS).load();
//		StreamSummary<T> res = ds.select(col(col)).as(encoder).javaRDD().aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
//		return res.topKSpaceSaving(k);
//	}

	/**
	 * calculate the top k elements using Spark SQL instead of a StreamSummary
	 * @param col
	 * @param k
	 * @param <T>
	 * @return a list of {@link Counter} representing the top item with the count
	 */
	public <T extends Serializable> List<Counter<T>> topKSparkSQL(String col, int k) {
		// no prepared statement available (C* Connector prevents injection, etc.)
		Dataset<Row> ds = sqlContext
				.sql(String.format("SELECT %s, COUNT(*) as count FROM data GROUP BY %s ORDER BY count DESC LIMIT %d", col, col, k));

		return ds.collectAsList().stream()
				.map(row -> new Counter<>((T) row.get(0), row.getLong(1)))
				.collect(Collectors.toList());
	}

	public <T extends Serializable> CompletableFuture<List<Counter<T>>> topKSparkSQLAsync(String col, int k) {
		return CompletableFuture.supplyAsync(() -> topKSparkSQL(col, k));
	}

	public List<com.dynatrace.msc.dore.query.Counter<String>> topKCountMinSketch(String col, int k, double eps, double confidence, int capacity) {
		CountMinSketchX cms = data.select(col(col)).as(Encoders.STRING()).javaRDD().mapPartitions(it -> {
			CountMinSketchX tmp = new CountMinSketchX(eps, confidence, 42, capacity);
			while (it.hasNext()) {
				tmp.add(it.next(), 1);
			}
			return Collections.singleton(tmp).iterator();
		}).reduce(CountMinSketchX::mergeX);
		return cms.topK(k).entrySet().stream().map(e -> new CounterImpl<>(e.getKey(), e.getValue()))
				.collect(Collectors.toList());
	}

	public CompletableFuture<List<com.dynatrace.msc.dore.query.Counter<String>>> topKCountMinSketchAsync(String col, int k, double eps, double confidence, int capacity) {
		return CompletableFuture.supplyAsync(() -> topKCountMinSketch(col, k, eps, confidence, capacity));
	}

	public Map<String, Long> topKCMS(String col, int k) {
		double eps = 0.1; double confidence = 0.9; int seed = 42;
		CountMinSketchX out = data.select(col(col)).as(Encoders.STRING()).javaRDD().mapPartitions(it -> {
			CountMinSketchX tmp = new CountMinSketchX(eps, confidence, seed, k + 10);
			while (it.hasNext()) {
				tmp.add(it.next(), 1L);
			}
			return Collections.singleton(tmp).iterator();
		}).reduce(CountMinSketchX::mergeX);

		return out.topK(k);
	}

	public <T extends Number> double median(String col, Encoder<T> encoder) {
		List<T> sorted = data.sort(col(col)).select(col(col)).as(encoder).collectAsList();
		if (sorted.size() % 2 != 0) {
			return (sorted.get(sorted.size() / 2).doubleValue() + sorted.get(sorted.size() / 2 - 1).doubleValue()) / 2;
		} else {
			return sorted.get(sorted.size() / 2).doubleValue();
		}
	}

	public <T extends Number> CompletableFuture<Double> medianAsync(String col, Encoder<T> encoder) {
		return CompletableFuture.supplyAsync(() -> median(col, encoder));
	}

	/**
	 * calculates the approximate quantiles using a variation of the Greenwald-Khanna algorithm
	 * @param col the numerical column from which that quantiles shall be calculated
	 * @param probabilities the quantile probabilities to calculate. Range is [0,1] (i.e. 0.5 is the median or 50th percentile)
	 * @param relativeError relative error that will be accepted. Set to 0 for exact calculation
	 * @return the percentile values for the given probabilities
	 */
	@Override
	public double[] percentilesGreenwaldKhanna(String col, double[] probabilities, double relativeError) {
		return data.stat().approxQuantile(col, probabilities, relativeError);
	}

	public CompletableFuture<double[]> percentilesGreenwaldKhannaAsync(String col, double[] probabilities, double relativeError) {
		return CompletableFuture.supplyAsync(() -> percentilesGreenwaldKhanna(col, probabilities, relativeError));
	}

	/**
	 * calculates the approximate quantiles for double values using a HDRHistogram
	 * <i>Note: HDRHistogram does not support negative values!</i>
	 * @param col the double/float column from which that quantiles shall be calculated
	 * @param probabilities the quantile probabilities to calculate. Range is [0,1] (i.e. 0.5 is the median or 50th percentile)
	 * @param significantValueDigits specifies the precision. Possible values are in the range 0..5
	 * @return the percentile values for the given probabilities
	 */
	public double[] percentilesHdrDouble(String col, double[] probabilities, @Nonnegative int significantValueDigits) {
		DoubleHistogram hg = hdrHistogramDouble(col, significantValueDigits);
		double[] percentiles = new double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++) {
			percentiles[i] = hg.getValueAtPercentile(probabilities[i] * 100);
		}
		return percentiles;
	}

	public CompletableFuture<double[]> percentilesHdrDoubleAsync(String col, double[] probabilities, @Nonnegative int significantValueDigits) {
		return CompletableFuture.supplyAsync(() -> percentilesHdrDouble(col, probabilities, significantValueDigits));
	}

	/**
	 * calculates the approximate quantiles for int values using a HDRHistogram
	 * @param col the integer column from which that quantiles shall be calculated
	 * @param probabilities the quantile probabilities to calculate. Range is [0,1] (i.e. 0.5 is the median or 50th percentile)
	 * @param significantValueDigits specifies the precision. Possible values are in the range 0..5
	 * @return the percentile values for the given probabilities
	 */
	public double[] percentilesHdrInt(String col, double[] probabilities, @Nonnegative int significantValueDigits) {
		Histogram hg = hdrHistogramInt(col, significantValueDigits);
		double[] percentiles = new double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++) {
			percentiles[i] = hg.getValueAtPercentile(probabilities[i] * 100);
		}
		return percentiles;
	}

	/**
	 * calculates the approximate quantiles for int values using a TDigest
	 * @param col the numerical column from which that quantiles shall be calculated
	 * @param probabilities the quantile probabilities to calculate. Range is [0,1] (i.e. 0.5 is the median or 50th percentile)
	 * @param compression the compression value. 100 would be efficient for regular use cases, 1000 is considered extremely large
	 * @return
	 */
	public double[] percentilesTDigest(String col, double[] probabilities, double compression) {
		TDigest td = data.select(col(col)).javaRDD().aggregate(TDigest.createDigest(compression), (digest, row) -> {
			Object rawValue = row.get(0);
			digest.add(Double.valueOf(rawValue.toString()));
			return digest;
		}, (d1, d2) -> {
			d1.add(d2);
			return d1;
		});

		double[] percentiles = new double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++) {
			percentiles[i] = td.quantile(probabilities[i]);
		}
		return percentiles;
	}

	public CompletableFuture<double[]> percentilesTDigestAsync(String col, double[] probabilities, double compression) {
		return CompletableFuture.supplyAsync(() -> percentilesTDigest(col, probabilities, compression));
	}

	public Histogram hdrHistogramInt(String col, int significantValueDigits) {
		return data.select(col(col)).javaRDD().aggregate(new Histogram(significantValueDigits), (hg, row) -> {
			hg.recordValue(row.getInt(0));
			return hg;
		}, (hg1, hg2) -> {
			hg1.add(hg2);
			return hg1;
		});
	}

	public Histogram hdrHistogramInt(String col, int significantValueDigits, int hightestTrackableValue) {
		return data.select(col(col)).javaRDD().aggregate(new Histogram(hightestTrackableValue, significantValueDigits), (hg, row) -> {
			hg.recordValue(row.getInt(0));
			return hg;
		}, (hg1, hg2) -> {
			hg1.add(hg2);
			return hg1;
		});
	}

	public DoubleHistogram hdrHistogramDouble(String col, int significantValueDigits) {
		return data.select(col(col)).javaRDD().aggregate(new DoubleHistogram(significantValueDigits), (hg, row) -> {
			Object rawValue = row.get(0);
			hg.recordValue(Double.valueOf(rawValue.toString()));
			return hg;
		}, (hg1, hg2) -> {
			hg1.add(hg2);
			return hg1;
		});
	}

	public DoubleHistogram hdrHistogramCasting(String col, int significantValueDigits) {
		return data.select(col(col)).javaRDD().aggregate(new DoubleHistogram(significantValueDigits), (hg, row) -> {
			Object val = row.get(0);
			hg.recordValue(Double.valueOf(val.toString()));
			return hg;
		}, (hg1, hg2) -> {
			hg1.add(hg2);
			return hg1;
		});
	}

	public DoubleHistogram hdrHistogramDoubleMapPartitions(String col, int significantValueDigits) {
		return data.select(col(col)).javaRDD().mapPartitions(it -> {
			DoubleHistogram hg = new DoubleHistogram(significantValueDigits);
			while (it.hasNext()) {
				hg.recordValue(it.next().getFloat(0));
			}
			return Collections.singleton(hg).iterator();
		}).reduce((hg1, hg2) -> {
			hg1.add(hg2);
			return hg1;
		});
	}

	@Override
	public Dataset<Row> querySql(String sql, String tempViewName) {
		data.createOrReplaceTempView(tempViewName);
		return sqlContext.sql(sql);
	}

	@Override
	/**
	 * calculate the cardinality of a given column using HyperLogLog
	 */
	public long cardinality(String col, double eps) {
		return data.select(col(col)).javaRDD().countApproxDistinct(eps);
	}

	public long cardinalityExact(String col) {
		return data.select(col(col)).javaRDD().distinct().count();
	}

	public <T> List<T> sql(String sql, Class<T> rowType) {
		Dataset<T> ds = sqlContext.sql(sql).as(Encoders.bean(rowType));
		return ds.collectAsList();
	}

	/**
	 * run a user defined function that follows the MapReduce paradigm
	 * @param mapFunction Map function that takes an iterator of a row and returns an iterator of the result type
	 *                    The function is called for each partition of the dataset representing the configured keyspace and table
	 *                    <pre>i.e.
	 *                    {@code
	 *                    it -> {
	 *                        T res = ...;
	 *                        while (it.hasNext()) {
	 *                            Row row = it.next();
	 *                            // handle rows in current partition
	 *                        }
	 *                        return Collections.singleton(res).iterator();
	 *                    }
	 *                    }
	 *                    </pre>
	 * @param reduceFunction Reduce function that takes two objects T and combines these objects to a single T object
	 *                       <pre>i.e.
	 *                       {@code
	 *                       (val1, val2) -> val1 + val2
	 *                       }
	 *                       </pre>
	 * @param <T> Return type of the final and intermediate results
	 * @return the final result computed with the Reduce function
	 */
	public <T> T udf(FlatMapFunction<Iterator<Row>, T> mapFunction, Function2<T, T, T> reduceFunction) {
		return data.javaRDD().mapPartitions(mapFunction).reduce(reduceFunction);
	}

	/**
	 * run a user defined function that follows the MapReduce paradigm
	 * @param mapFunction Map function that takes an iterator of a given RowType and returns an iterator of the result type
	 *                    The function is called for each partition of the dataset representing the configured keyspace and table
	 *                    <pre>i.e.
	 *                    {@code
	 *                    it -> {
	 *                        ReturnType res = ...;
	 *                        while (it.hasNext()) {
	 *                            RowType row = it.next();
	 *                            // handle rows in current partition
	 *                        }
	 *                        return Collections.singleton(res).iterator();
	 *                    }
	 *                    }
	 *                    </pre>
	 * @param reduceFunction Reduce function that takes two objects of ReturnType and combines these objects to a single ReturnType object
	 *                       <pre>i.e.
	 *                       {@code
	 *                       (val1, val2) -> val1 + val2
	 *                       }
	 *                       </pre>
	 * @param encoder {@link Encoder} object that is used to cast the {@link Row} object. i.e. {@code Encoders.bean(MyRowType.class)}
	 * @param <RowType> Class to which the dataset rows should be cast
	 * @param <ReturnType> Return type of the final and intermediate Results output
	 * @return the final result computed with the Reduce function
	 */
	public <RowType, ReturnType> ReturnType udf(FlatMapFunction<Iterator<RowType>, ReturnType> mapFunction,
			Function2<ReturnType, ReturnType, ReturnType> reduceFunction, Encoder<RowType> encoder) {

		return data.as(encoder).javaRDD().mapPartitions(mapFunction).reduce(reduceFunction);
	}


	public <RowType, ReturnType> ReturnType udfDs(MapPartitionsFunction<RowType, ReturnType> mapFunction,
			ReduceFunction<ReturnType> reduceFunction, Encoder<RowType> rowEncoder, Encoder<ReturnType> resultEncoder) {
		return data.as(rowEncoder).mapPartitions(mapFunction, resultEncoder).reduce(reduceFunction);
	}

	public <Intermediate, RowType> Intermediate udfAggregate(Intermediate zeroValue,
			Function2<Intermediate, RowType, Intermediate> mapFunction,
			Function2<Intermediate, Intermediate, Intermediate> reduceFunction,
			Encoder<RowType> rowEncoder) {

		return data.as(rowEncoder).javaRDD().aggregate(zeroValue, mapFunction, reduceFunction);
	}

	public SQLContext getSqlContext() {
		return sqlContext;
	}

	public Dataset<Row> getDataSet() {
		return data;
	}

	public static final class QueryExecutorBuilder {

		private SparkConf conf;
		private Map<String, String> options = new HashMap<>(2);
		private String masterUrl;

		private QueryExecutorBuilder() {
			this.conf = new SparkConf()
					.setMaster(SPARK_MASTER)
					.setJars(getJars())
					.set("spark.cassandra.connection.host", "localhost")
					.set("spark.eventLog.enabled", "true")
					.set("spark.eventLog.dir", "file:/tmp/spark-events")
					.set("spark.history.fs.logDirectory", "file:/tmp/spark-events")
					.set("spark.scheduler.mode", "FAIR")
					.set("spark.pushdown", "true");

			options.put("keyspace", "kp_test");
			options.put("table", "test_data");

			this.masterUrl = SPARK_MASTER;
		}

		public QueryExecutorBuilder sparkConf(final SparkConf conf) {
			this.conf = conf;
			return this;
		}

		public QueryExecutorBuilder options(final Map<String, String> options) {
			this.options = options;
			return this;
		}

		public QueryExecutorBuilder master(final String masterUrl) {
			this.masterUrl = masterUrl;
			return this;
		}

		public QueryExecutorBuilder mainTable(final String keyspace, final String table) {
			this.options.put("keyspace", keyspace);
			this.options.put("table", table);
			return this;
		}

		public QueryExecutor build() {
			try {
				return new QueryExecutor(conf, options, masterUrl);
			} catch (Exception e) {
				// ignore for now ...
			}
			return null;
		}

	}

	public static final class Counter<T extends Serializable> implements com.dynatrace.msc.dore.query.Counter<T> {
		private T item;
		private long count;

		private Counter(T item, long count) {
			this.item = item;
			this.count = count;
		}

		@Override
		public T getItem() {
			return item;
		}

		@Override
		public long getCount() {
			return count;
		}

		@Override
		public long getError() {
			return -1;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder();
			sb.append(item).append(": ").append(count);
			return sb.toString();
		}
	}
}
