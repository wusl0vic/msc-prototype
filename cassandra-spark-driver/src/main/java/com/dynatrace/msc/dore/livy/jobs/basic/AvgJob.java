package com.dynatrace.msc.dore.livy.jobs.basic;

import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.functions;

public class AvgJob extends BaseJob<Double> {

	private final String col;

	public AvgJob(String keyspace, String table, String col) {
		this.col = col;
		setOptions(keyspace, table);
	}

	@Override
	public Double call(JobContext ctx) throws Exception {
		return getDataset(ctx).select(functions.avg(col)).as(Encoders.DOUBLE()).first();
	}
}
