package com.dynatrace.msc.dore.query;

import java.io.Serializable;

public class CounterImpl<T extends Serializable> implements Counter<T> {

	private T item;
	private long count;
	private long error;

	public CounterImpl(T item, long count, long error) {
		this.item = item;
		this.count = count;
		this.error = error;
	}

	public CounterImpl(T item) {
		this(item, 1, -1);
	}

	public CounterImpl(T item, long count) {
		this(item, count, -1);
	}

	@Override
	public T getItem() {
		return item;
	}

	@Override
	public long getCount() {
		return count;
	}

	@Override
	public long getError() {
		return error;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(item).append(": ").append(count);
		if (error > -1) {
			sb.append(" (error: ").append(error).append(")");
		}
		return sb.toString();
	}
}
