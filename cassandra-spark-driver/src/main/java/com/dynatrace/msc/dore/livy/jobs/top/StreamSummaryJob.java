package com.dynatrace.msc.dore.livy.jobs.top;

import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.livy.jobs.BaseJob;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Encoder;

import java.io.Serializable;

import static org.apache.spark.sql.functions.col;

public class StreamSummaryJob<T extends Serializable> extends BaseJob<StreamSummary<T>> {

	private final String col;
	private final int capacity;
	private final Encoder<T> encoder;

	public StreamSummaryJob(String keyspace, String table, String col, int capacity, Encoder<T> encoder) {
		this.col = col;
		this.capacity = capacity;
		this.encoder = encoder;
		setOptions(keyspace, table);
	}

	@Override
	public StreamSummary<T> call(JobContext ctx) throws Exception {
		return getDataset(ctx).select(col(col)).as(encoder).javaRDD()
				.aggregate(new StreamSummary<>(capacity), StreamSummary::offerAndReturn, StreamSummary::merge);
	}
}
