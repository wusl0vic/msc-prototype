package com.dynatrace.msc.dore.query;

import java.io.Serializable;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Row;

import com.dynatrace.msc.dore.algorithm.StreamSummary;

public interface QueryFunctions {

	long sum(final String col);
	long min(final String col);
	long max(final String col);
	double avg(final String col);

	/**
	 * calculate the top k elements
	 * @param col the column from which the top k elements shall be calculated
	 * @param k the number of top items
	 * @param capacity size of the Stream Summary. Higher value increases accuracy on the cost of storage
	 * @param encoder Encoder used to cast the result. see {@link Encoder}
	 * @param <T>
	 * @return a list of {@link StreamSummary.Counter} that represent the top elements including count and error
	 */
	<T extends Serializable> List<StreamSummary<T>.Counter<T>> topK(final String col, final int k, final int capacity, Encoder<T> encoder);

	/**
	 * calculate the approximate quantiles using the Greenwhald-Khanna Algorithm
	 * @param col the column from which that quantiles shall be calculated
	 * @param percentiles which quantiles to calculate
	 * @param relativeError
	 * @return
	 */
	double[] percentilesGreenwaldKhanna(final String col, final double[] percentiles, final double relativeError);

	Dataset<Row> querySql(final String sql, final String tempViewName);

	long cardinality(final String col, final double eps);
}
