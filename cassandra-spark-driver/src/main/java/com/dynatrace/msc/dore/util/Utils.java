package com.dynatrace.msc.dore.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public final class Utils {

	public static void shutdownAndAwaitTermination(final ExecutorService es) {
		es.shutdown();
		try {
			if (!es.awaitTermination(60, TimeUnit.SECONDS)) {
				es.shutdownNow();
				if (!es.awaitTermination(60, TimeUnit.SECONDS)) {
					System.err.println("ExecutorService did not terminate");
				}
			}
		} catch (InterruptedException e) {
			es.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

}
