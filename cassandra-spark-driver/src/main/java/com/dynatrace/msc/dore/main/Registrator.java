package com.dynatrace.msc.dore.main;

import org.apache.spark.serializer.KryoRegistrator;

import com.dynatrace.msc.dore.algorithm.CountMinSketchX;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.domain.City;
import com.dynatrace.msc.dore.domain.RealEstate;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.tdunning.math.stats.AVLTreeDigest;
import com.tdunning.math.stats.MergingDigest;
import com.tdunning.math.stats.TDigest;

public class Registrator implements KryoRegistrator {

	@Override
	public void registerClasses(Kryo kryo) {
		kryo.register(TDigest.class, new FieldSerializer(kryo, TDigest.class));
		kryo.register(AVLTreeDigest.class, new FieldSerializer(kryo, AVLTreeDigest.class));
		kryo.register(MergingDigest.class, new FieldSerializer(kryo, MergingDigest.class));
		kryo.register(StreamSummary.class, new FieldSerializer(kryo, StreamSummary.class));
		kryo.register(StreamSummary.Bucket.class, new FieldSerializer(kryo, StreamSummary.Bucket.class));
		kryo.register(StreamSummary.Counter.class, new FieldSerializer(kryo, StreamSummary.Counter.class));
//		kryo.register(StreamSummary.class);
//		kryo.register(QueryExecutor.class, new FieldSerializer(kryo, QueryExecutor.class));
		kryo.register(RealEstate.class, new FieldSerializer(kryo, RealEstate.class));
		kryo.register(City.class, new FieldSerializer(kryo, City.class));
		kryo.register(CountMinSketchX.class, new FieldSerializer(kryo, CountMinSketchX.class));
	}
}
