package com.dynatrace.msc.dore.spring.controllers.dto;

import io.swagger.annotations.ApiModelProperty;

public class TopKRequest {

	@ApiModelProperty(
			notes = "The algorithm used to calculate the Top K elements",
			allowableValues = "SPACE_SAVING, COUNT_MIN_SKETCH",
			required = true)
	private TopKAlgorithm algorithm;

	@ApiModelProperty(notes = "Length of the supervised elements in SpaceSaving as well as CMS")
	private Integer capacity;

	@ApiModelProperty(notes = "Parameter for the Count-Min-Sketch")
	private Double epsilon;

	@ApiModelProperty(notes = "Parameter for the Count-Min-Sketch")
	private Double confidence;

	public TopKRequest() {

	}

	public TopKAlgorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(TopKAlgorithm algorithm) {
		this.algorithm = algorithm;
	}

	public Double getEpsilon() {
		return epsilon;
	}

	public void setEpsilon(Double epsilon) {
		this.epsilon = epsilon;
	}

	public Double getConfidence() {
		return confidence;
	}

	public void setConfidence(Double confidence) {
		this.confidence = confidence;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
}
