package com.dynatrace.msc.dore.livy;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static com.dynatrace.msc.dore.util.SparkConstants.USERSESSION_TABLE_OPTIONS;
import static java.util.Collections.unmodifiableMap;
import static org.apache.spark.sql.functions.col;
import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.livy.Job;
import org.apache.livy.JobContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.dynatrace.msc.dore.query.Counter;

public class UserSessionTest {

	private static LivyExecutor staticExecutor;

	private static final String KP = "kp_test";
	private static final String TABLE = "usersession";

	private static final Map<String, String> options;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("keyspace", KP);
		tmp.put("table", TABLE);
		options = unmodifiableMap(tmp);
	}

	@BeforeClass
	public static void beforeClass() throws Exception {
		staticExecutor = LivyExecutor.buildNonSpring(USERSESSION_TABLE_OPTIONS);
	}

	@AfterClass
	public static void afterClass() {
		staticExecutor.stop(true);
	}

	@Test
	public void testCount() {
		CompletableFuture<Long> future = staticExecutor.count(KP, TABLE);
		future.thenAccept(n -> System.out.println("There are " + n + " sessions"));
		future.join();
	}

	@Test
	@Ignore // NPE on CMSX.add -> CMS.add -> Filter.getHashBuckets
	public void topKCMSTest() {
		CompletableFuture<List<Counter<String>>> future = staticExecutor.topKCountMinSketch(KP, TABLE, "city", 10, 100, 0.1, 0.9);
		future.thenAccept(top -> top.forEach(System.out::println));
		future.join();
	}

	@Test
	public void topKTest() throws Exception {
		staticExecutor.count(KP, TABLE).get();
		long now = System.currentTimeMillis();
		CompletableFuture<List<Counter<String>>> future1 = staticExecutor.topKSpaceSaving(KP, TABLE, "city", 10, 500);
		future1.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Space saving: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture<List<Counter<String>>> future2 = staticExecutor.topKExact(KP, TABLE, "city", 10);
		future2.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Exact: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture<List<Counter<String>>> future3 = staticExecutor.topKCountMinSketch(KP, TABLE, "city", 10, 200, 0.01, 0.99);
		future3.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("CMS: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture.allOf(future1, future2, future3).join();
	}

	@Test
	public void testMedian() throws Exception {
		long maxDuration = staticExecutor.max(KP, TABLE, "duration").get();
//		System.out.println("max duration = " + maxDuration);

		long now = System.currentTimeMillis();
		CompletableFuture<double[]> future1 = staticExecutor.percentilesTD(KP, TABLE, "duration", new double[] { 0.5 }, 200);
		future1.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("median TD    %.4f (%d ms)%n", p[0], end - now);
		});

		CompletableFuture<double[]> future2 = staticExecutor.percentilesGK(KP, TABLE, "duration", new double[] { 0.5 }, 0.0);
		future2.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("median GK    %.4f (%d ms)%n", p[0], end - now);
		});

		CompletableFuture<double[]> futureHdr = staticExecutor.percentilesHDR(KP, TABLE, "duration", new double[] { 0.5 }, 5, maxDuration);
		futureHdr.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("median HDR   %.4f (%d ms)%n", p[0], end - now);
		});

		CompletableFuture<Double> future3 = staticExecutor.median(KP, TABLE, "duration");
		future3.thenAccept(m -> {
			long end = System.currentTimeMillis();
			System.out.printf("median exact %.4f (%d ms)%n", m, end - now);
		});

		CompletableFuture.allOf(future1, future2, future3, futureHdr).join();
	}

	private class CustomSumJob implements Job<Long> {

		@Override
		public Long call(JobContext ctx) throws Exception {
			Dataset<Row> ds = ctx.sqlctx().read().format(SPARK_CASSANDRA_FORMAT)
					.options(options).load();
			return ds.select(col("userActionCount")).javaRDD().mapPartitions(it -> {
				long sum = 0;
				while (it.hasNext()) {
					sum += it.next().getInt(0);
				}
				return Collections.singleton(sum).iterator();
			}).reduce(Long::sum);
		}
	}

	@Test
	public void testCustomJob() throws Exception {
		long sum = staticExecutor.sum(KP, TABLE, "userActionCount").get();
		long customSum = staticExecutor.submitJob(new CustomSumJob()).get();

		System.out.printf("Sum = %d, custom sum = %d%n", sum, customSum);
		assertEquals(sum, customSum);
	}

}
