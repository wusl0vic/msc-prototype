package com.dynatrace.msc.dore.query;

import static com.dynatrace.msc.dore.util.TimeUtils.measureTime;
import static com.dynatrace.msc.dore.util.Utils.shutdownAndAwaitTermination;
import static org.apache.spark.sql.functions.col;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.HdrHistogram.DoubleHistogram;
import org.HdrHistogram.Histogram;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.dynatrace.msc.dore.AbstractBaseTest;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.domain.RealEstate;

public class QueryExecutorTest extends AbstractBaseTest {

	private static QueryExecutor executor;

	@BeforeClass
	public static void beforeClass() {
		AbstractBaseTest.beforeClass();

		executor = QueryExecutor.builder()
				.sparkConf(conf)
				.options(CASSANDRA_OPTIONS)
				.build();
	}

	@Test
	public void testBasicFunctions() throws Exception {
		long minBaths = measureTime(() -> executor.min("baths"));
		long maxBaths = measureTime(() -> executor.max("baths"));

		assertEquals(0, minBaths);
		assertEquals(5, maxBaths);

		double avgBaths = executor.avg("baths");
		long sumBaths = executor.sum("baths");

		assertErrorSmallerThanEpsilon(avgBaths, 1.790456, 0.0001);
		assertEquals(1726, sumBaths);
	}

	@Test
	public void testSqlQuery() {
		List<RealEstate> res = executor.sql("SELECT * FROM data WHERE baths = 5", RealEstate.class);
		assertEquals(2,res.size());
	}

	@Test
	public void testStreamSummary() {
		List<StreamSummary<String>.Counter<String>> res = executor.topK("city", 4, 20, Encoders.STRING());
		assertEquals(4, res.size());

		List<StreamSummary<Integer>.Counter<Integer>> summary = executor.topK("beds", 5, 20, Encoders.INT());
		summary.forEach(System.out::println);

		executor.getSqlContext().sql("SELECT beds, COUNT(*) as count FROM data GROUP BY beds ORDER BY count DESC LIMIT 5").show();
	}

	@Test
	public void testCardinality() {
		for (int i = 0; i < 3; i++) {
			long distinctCities = executor.cardinality("city", 0.1);
			long exactCities = executor.getDataSet().select(col("city")).distinct().count();
			long exactCities2 = executor.cardinalityExact("city");
			assertEquals(exactCities, exactCities2);
			assertErrorSmallerThanEpsilon(distinctCities, exactCities, 0.1);
		}
	}

	@Test
	public void testHdrHistogram() {
		System.out.println("=== latitude ===");
		double[] percentiles = executor.percentilesGreenwaldKhanna("latitude", new double[] { 0.1, 0.5, 0.99 }, 0);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		System.out.println();

		DoubleHistogram hg = executor.hdrHistogramDouble("latitude", 5);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", hg.getValueAtPercentile(10), hg.getValueAtPercentile(50), hg.getValueAtPercentile(99));

		System.out.println();

		hg = executor.hdrHistogramDoubleMapPartitions("latitude", 5);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", hg.getValueAtPercentile(10), hg.getValueAtPercentile(50), hg.getValueAtPercentile(99));

		percentiles = executor.percentilesHdrDouble("latitude", new double[] {0.1, 0.5, 0.99}, 5);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

//		System.out.println("=== longitude ===");
//		percentilesGreenwaldKhanna = executor.percentilesGreenwaldKhanna("longitude", new double[] { 0.1, 0.5, 0.99 }, 0.001);
//		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentilesGreenwaldKhanna[0], percentilesGreenwaldKhanna[1], percentilesGreenwaldKhanna[2]);
//
//		hg = executor.hdrHistogramDouble("longitude", 5);
//		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", hg.getValueAtPercentile(0.1), hg.getValueAtPercentile(0.5), hg.getValueAtPercentile(0.99));
	}

	@Test
	public void testHdrHistogramInt() {
		System.out.println("=== price ===");
		double[] percentiles = executor.percentilesGreenwaldKhanna("price", new double[] { 0.1, 0.5, 0.99 }, 0);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		Histogram hg = executor.hdrHistogramInt("price", 5);
		System.out.printf("0.1: %d, 0.5: %d, 0.99: %d%n", hg.getValueAtPercentile(10), hg.getValueAtPercentile(50), hg.getValueAtPercentile(99));

		System.out.println("=== beds ===");
		percentiles = executor.percentilesGreenwaldKhanna("beds", new double[] { 0.1, 0.5, 0.99 }, 0);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		hg = executor.hdrHistogramInt("beds", 5);
		System.out.printf("0.1: %d, 0.5: %d, 0.99: %d%n", hg.getValueAtPercentile(10), hg.getValueAtPercentile(50), hg.getValueAtPercentile(99));

		percentiles = executor.percentilesHdrInt("beds", new double[] {0.1, 0.5, 0.99}, 5);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);
	}

	@Test
	public void testTDigest() {
		System.out.println("=== latitude ===");
		double[] percentiles = executor.percentilesGreenwaldKhanna("latitude", new double[] { 0.1, 0.5, 0.99 }, 0);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		percentiles = executor.percentilesTDigest("latitude", new double[] { 0.1, 0.5, 0.99 }, 100);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		System.out.println("=== longitude ===");
		percentiles = executor.percentilesGreenwaldKhanna("longitude", new double[] { 0.1, 0.5, 0.99 }, 0);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		percentiles = executor.percentilesTDigest("longitude", new double[] { 0.1, 0.5, 0.99 }, 100);
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);
	}

	@Test
	public void testComparePercentile() {
		double[] probabilities = new double[] { 0.1, 0.5, 0.99 };
		double[] percentiles = executor.percentilesGreenwaldKhanna("latitude", probabilities, 0.0);
		System.out.println("Greenwald-Khanna");
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);

		double[] percentilesHdr = executor.percentilesHdrDouble("latitude", probabilities, 5);
		System.out.println("HDR Histogram");
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentilesHdr[0], percentilesHdr[1], percentilesHdr[2]);

		double[] percentilesTD = executor.percentilesTDigest("latitude", probabilities, 100);
		System.out.println("TDigest");
		System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentilesTD[0], percentilesTD[1], percentilesTD[2]);

		for (int i = 0; i < percentiles.length; i++) {
			double errorHdr = Math.abs(percentilesHdr[i] - percentiles[i]) / percentiles[i];
			double errorTd = Math.abs(percentilesTD[i] - percentiles[i]) / percentiles[i];

			System.out.printf("Error %.2f: hdr - %.4f; td - %.4f%n", probabilities[i], errorHdr, errorTd);
		}
	}

	@Test
	public void testMedian() {
		double median = executor.median("latitude", Encoders.FLOAT());
		double medianHdr = executor.percentilesHdrDouble("latitude", new double[] { 0.5 }, 5)[0];

		System.out.printf("median: %.4f (hdr: %.4f)%n", median, medianHdr);
		assertErrorSmallerThanEpsilon(medianHdr, median, 0.01);
	}

	@Test
	public void testCompareMedianAsync() throws InterruptedException, ExecutionException {
		final long now = System.currentTimeMillis();
		CompletableFuture<Double> medianFuture = executor.medianAsync("latitude", Encoders.FLOAT());
		medianFuture.thenAccept(m -> {
			final long end = System.currentTimeMillis();
			System.out.printf("calculated median %.4f. took %d ms%n", m, end - now);
		});
		CompletableFuture<double[]> medianHdrFuture = executor.percentilesHdrDoubleAsync("latitude", new double[] { 0.5 }, 5);

		medianHdrFuture.thenAccept(m -> {
			final long end = System.currentTimeMillis();
			System.out.printf("calculated HDR median %.4f. took %d ms%n", m[0], end - now);
		});

		CompletableFuture<double[]> medianTDFuture = executor.percentilesTDigestAsync("latitude", new double[] { 0.5 }, 100);
		medianTDFuture.thenAccept(m -> {
			final long end = System.currentTimeMillis();
			System.out.printf("calculated TD  median %.4f. took %d ms%n", m[0], end - now);
		});

		CompletableFuture<double[]> medianGKFuture = executor.percentilesGreenwaldKhannaAsync("latitude", new double[] { 0.5 }, 0.01);
		medianGKFuture.thenAccept(m -> {
			final long end = System.currentTimeMillis();
			System.out.printf("calculated GK  median %.4f. took %d ms%n", m[0], end - now);
		});

		double median = medianFuture.get();
		double medianHdr = medianHdrFuture.get()[0];
		double medianTD = medianTDFuture.get()[0];
		double medianGK = medianGKFuture.get()[0];

		assertErrorSmallerThanEpsilon(medianHdr, median, 0.01);
		assertErrorSmallerThanEpsilon(medianTD, median, 0.01);
		assertErrorSmallerThanEpsilon(medianGK, median, 0.01);
	}

	@Test
	public void testCompareTopKAsync() throws Exception {
		final long now = System.currentTimeMillis();

		CompletableFuture<List<QueryExecutor.Counter<String>>> topKSqlFuture = executor.topKSparkSQLAsync("city", 5);
		CompletableFuture<List<StreamSummary<String>.Counter<String>>> topKFuture = executor.topKAsync("city", 5, 40, Encoders.STRING());
		CompletableFuture<List<Counter<String>>> topKCmsFuture = executor.topKCountMinSketchAsync("city", 5, 0.01, 0.99, 30);

		topKSqlFuture.thenAccept(l -> {
			final long end = System.currentTimeMillis();
			System.out.println("Spark SQL took " + (end - now) + " ms");
			l.forEach(System.out::println);
			System.out.println();
		});

		topKFuture.thenAccept(l -> {
			final long end = System.currentTimeMillis();
			System.out.println("Space Saving took " + (end - now) + " ms");
			l.forEach(System.out::println);
			System.out.println();
		});

		topKCmsFuture.thenAccept(l -> {
			final long end = System.currentTimeMillis();
			System.out.println("CMS took " + (end - now) + " ms");
			l.forEach(System.out::println);
			System.out.println();
		});

		List<QueryExecutor.Counter<String>> topKSql = topKSqlFuture.get();
		List<StreamSummary<String>.Counter<String>> topkSpaceSaving = topKFuture.get();
		List<Counter<String>> topKCms = topKCmsFuture.get();

		assertTop5TestCities(topKSql.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount())).collect(Collectors.toList()));
		assertTop5TestCities(topkSpaceSaving.stream().map(c -> new CounterImpl<>(c.getItem(), c.getCount())).collect(Collectors.toList()));
		assertTop5TestCities(topKCms);
	}

	@Test
	@Ignore
	public void testTopKCMS() {
		Map<String, Long> res = executor.topKCMS("city", 4);
		res.forEach((city, count) -> System.out.println(city + ": " + count));
	}

	// StreamSummary seems to be faster
	// although the first run always takes longer
	@Test
	public void testCompareTopK() {
		String[] cols = new String[] { "city", "zip" };
		for (int i = 0; i < 3; i++) {
			for (String col : cols) {
				List<QueryExecutor.Counter<String>> topKSparkSql = executor.topKSparkSQL(col, 4);
				List<StreamSummary<String>.Counter<String>> topKStreamSummary = executor.topK(col, 4, 50, Encoders.STRING());

				List<String> topStreamSummary = topKStreamSummary.stream().map(StreamSummary.Counter::getItem)
						.collect(Collectors.toList());
				List<String> topSparkSql = topKSparkSql.stream().map(QueryExecutor.Counter::getItem).collect(Collectors.toList());

				assertEquals(4, topSparkSql.size());
				assertEquals(4, topStreamSummary.size());
				topSparkSql.forEach(city -> assertTrue(topStreamSummary.contains(city)));
				topKStreamSummary.forEach(System.out::println);
			}
		}
	}

	@Test
	public void testUDF() {
		int count = executor.udf(new FlatMapFunction<Iterator<Row>, Integer>() {

			@Override
			public Iterator<Integer> call(Iterator<Row> it) throws Exception {
				int c = 0;
				while (it.hasNext()) {
					c++;
					it.next();
				}
				return Collections.singleton(c).iterator();
			}
		}, new Function2<Integer, Integer, Integer>() {

			@Override
			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1 + v2;
			}
		});

		int countLambda = executor.udf(it -> {
			int c = 0;
			while (it.hasNext()) {
				c++;
				it.next();
			}
			return Collections.singleton(c).iterator();
		}, Integer::sum);

		assertEquals(964, count);
		assertEquals(964, countLambda);
		System.out.println(count + " <-> " + countLambda);
	}

	@Test
	public void testUDFTyped() {
		int sumBaths = executor.udf(new FlatMapFunction<Iterator<RealEstate>, Integer>() {

			@Override
			public Iterator<Integer> call(Iterator<RealEstate> it) throws Exception {
				int i = 0;
				while (it.hasNext()) {
					RealEstate estate = it.next();
					i += estate.getBaths();
				}
				return Collections.singleton(i).iterator();
			}
		}, new Function2<Integer, Integer, Integer>() {

			@Override
			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1 + v2;
			}
		}, Encoders.bean(RealEstate.class));

		int sumBathsLamda = executor.udf(it -> {
			int i = 0;
			while (it.hasNext()) {
				i += it.next().getBaths();
			}
			return Collections.singleton(i).iterator();
		}, Integer::sum, Encoders.bean(RealEstate.class));

		long sumAgg = executor.sum("baths");

		assertEquals(1726, sumBaths);
		assertEquals(1726, sumAgg);
		assertEquals(1726, sumBathsLamda);

		System.out.println(sumBaths);
		System.out.println(sumAgg);
	}

	@Test
	public void testUDFWithoutRDDConversion() {
		int sumBaths = executor.udfDs(new MapPartitionsFunction<RealEstate, Integer>() {

			@Override
			public Iterator<Integer> call(Iterator<RealEstate> it) throws Exception {
				int sum = 0;
				while (it.hasNext()) {
					RealEstate estate = it.next();
					sum += estate.getBaths();
				}
				return Collections.singleton(sum).iterator();
			}
		}, new ReduceFunction<Integer>() {

			@Override
			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1 + v2;
			}
		}, Encoders.bean(RealEstate.class), Encoders.INT());

		int sumBathsLambda = executor.udfDs(it -> {
			int sum = 0;
			while (it.hasNext()) {
				RealEstate estate = it.next();
				sum += estate.getBaths();
			}
			return Collections.singleton(sum).iterator();
		}, Integer::sum, Encoders.bean(RealEstate.class), Encoders.INT());

		assertEquals(1726, sumBaths);
		assertEquals(1726, sumBathsLambda);
	}

	@Test
	public void testUDFAggregate() {
		int sumBaths = executor.udfAggregate(0, (sum, estate) -> sum += estate.getBaths(), Integer::sum, Encoders.bean(RealEstate.class));
		assertEquals(1726, sumBaths);

		AvgIntermediate res = executor.udfAggregate(AvgIntermediate.zero(), (avg, estate) -> {
			avg.count++;
			avg.sum += estate.getBaths();
			return avg;
		}, (avg1, avg2) -> {
			avg1.sum += avg2.sum;
			avg1.count += avg2.count;
			return avg1;
		}, Encoders.bean(RealEstate.class));

		double avgExact = executor.avg("baths");
		double avgUdf = res.sum / (double) res.count;
		System.out.println("avg = " + avgExact);
		System.out.println("avg udf = " + avgUdf);
		assertErrorSmallerThanEpsilon(avgUdf, avgExact, 0.00000001);
	}

	private static class AvgIntermediate implements Serializable {
		private int sum;
		private int count;

		static AvgIntermediate zero() {
			return new AvgIntermediate(0, 0);
		}

		AvgIntermediate(int sum, int count) {
			this.sum = sum;
			this.count = count;
		}
	}

	@Test
	public void testCompareUdfImplementations() {
//		int sumAgg = (int)executor.sum("baths");

		int sumUdfRow = executor.udf(it -> {
			int sum = 0;
			while (it.hasNext()) {
				Row row = it.next();
				sum += row.getInt(1); // baths is column w/ index 1
			}
			return Collections.singleton(sum).iterator();
		}, Integer::sum);

		int sumUdfTyped = executor.udf(it -> {
			int sum = 0;
			while (it.hasNext()) {
				sum += it.next().getBaths();
			}
			return Collections.singleton(sum).iterator();
		}, Integer::sum, Encoders.bean(RealEstate.class));

		int sumUdfNoRdd = executor.udfDs(it -> {
			int sum = 0;
			while (it.hasNext()) {
				sum += it.next().getBaths();
			}
			return Collections.singleton(sum).iterator();
		}, Integer::sum, Encoders.bean(RealEstate.class), Encoders.INT());

		int sumUdfAgg = executor.udfAggregate(0,
				(sum, estate) -> sum += estate.getBaths(),
				Integer::sum,
				Encoders.bean(RealEstate.class));


//		assertEquals(1726, sumAgg);
		assertEquals(1726, sumUdfRow);
		assertEquals(1726, sumUdfTyped);
		assertEquals(1726, sumUdfNoRdd);
		assertEquals(1726, sumUdfAgg);
	}

	@Test
	public void testConcurrentJobs() {
		// create 3 threads that run a udf
		ExecutorService es = Executors.newFixedThreadPool(3);
		es.submit(() -> {
			executor.udf(it -> {
				int sum = 0;
				while (it.hasNext()) {
					Row row = it.next();
					sum += row.getInt(1); // baths is column w/ index 1
				}
				return Collections.singleton(sum).iterator();
			}, Integer::sum);
		});

		es.submit(() -> {
			executor.udf(it -> {
				int sum = 0;
				while (it.hasNext()) {
					sum += it.next().getBaths();
				}
				return Collections.singleton(sum).iterator();
			}, Integer::sum, Encoders.bean(RealEstate.class));
		});

		es.submit(() -> {
			executor.udfDs(it -> {
				int sum = 0;
				while (it.hasNext()) {
					sum += it.next().getBaths();
				}
				return Collections.singleton(sum).iterator();
			}, Integer::sum, Encoders.bean(RealEstate.class), Encoders.INT());
		});

		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testSerialAggJobs() {
		executor.avg("beds");
		executor.avg("baths");
		executor.avg("sq__ft");
	}

	@Test
	public void testParallelAggJobs() {
		ExecutorService es = Executors.newFixedThreadPool(3);
		es.submit(() -> executor.avg("beds"));
		es.submit(() -> executor.avg("baths"));
		es.submit(() -> executor.avg("sq__ft"));
		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testCompareSerialThenParallelAggJobs() {
		executor.avg("beds");
		executor.avg("baths");
		executor.avg("sq__ft");

		ExecutorService es = Executors.newFixedThreadPool(3);
		es.submit(() -> executor.avg("beds"));
		es.submit(() -> executor.avg("baths"));
		es.submit(() -> executor.avg("sq__ft"));
		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testCompareParallelThenSerialAggJobs() {
		ExecutorService es = Executors.newFixedThreadPool(3);
		es.submit(() -> executor.avg("beds"));
		es.submit(() -> executor.avg("baths"));
		es.submit(() -> executor.avg("sq__ft"));
		shutdownAndAwaitTermination(es);

		executor.avg("beds");
		executor.avg("baths");
		executor.avg("sq__ft");
	}

	@Test
	public void testCompareMapPartitionsAggregate() {
		executor.getSqlContext().sql("SELECT city, COUNT(*) as count FROM data GROUP BY city ORDER BY count DESC LIMIT 4").show();

		List<StreamSummary<String>.Counter<String>> topKMapPartitions = executor.topK("city", 4, 50, Encoders.STRING());
		List<StreamSummary<String>.Counter<String>> topKAggregate = executor.topKAggregate("city", 4, 50, Encoders.STRING());

		System.out.println("MapPartitions:");
		topKMapPartitions.forEach(System.out::println);

		System.out.println("Aggregate:");
		topKAggregate.forEach(System.out::println);
	}

	@Test
	public void testMultipleQueryExecutors() {
		ExecutorService es = Executors.newFixedThreadPool(2);

		es.submit(() -> {
			QueryExecutor qry1 = QueryExecutor.builder().build();
			double[] percentiles = qry1.percentilesTDigest("beds", new double[] { 0.01, 0.5, 0.99 }, 100);
			System.out.println("beds:");
			System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);
		});

		es.submit(() -> {
			QueryExecutor qry2 = QueryExecutor.builder().build();
			double[] percentiles = qry2.percentilesGreenwaldKhanna("baths", new double[] { 0.01, 0.5, 0.99 }, 0.1);
			System.out.println("baths:");
			System.out.printf("0.1: %.2f, 0.5: %.2f, 0.99: %.2f%n", percentiles[0], percentiles[1], percentiles[2]);
		});

		shutdownAndAwaitTermination(es);
	}

}