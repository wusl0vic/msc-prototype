package com.dynatrace.msc.dore;

import static com.dynatrace.msc.dore.util.Utils.shutdownAndAwaitTermination;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;

import com.dynatrace.msc.dore.livy.LivyExecutor;
import com.dynatrace.msc.dore.query.Counter;
import com.dynatrace.msc.dore.query.DirectExecutor;
import com.dynatrace.msc.dore.query.QueryExecutorTest;
import com.dynatrace.msc.dore.query.SparkExecutor;

public class TestParallel {

	@Test
	public void test() {
		Class[] cls = { QueryExecutorTest.class };
		JUnitCore.runClasses(ParallelComputer.methods(), cls);
	}

	@Test
	public void testCompareLivyDirect() throws Exception {
		String kp = "kp_test";
		String table = "usersession";
		String col = "duration";
		double[] prob = new double[] { 0.5 };


		ExecutorService es = Executors.newWorkStealingPool();
		es.submit(() -> {
			final long now = System.currentTimeMillis();
			try (SparkExecutor livy = LivyExecutor.buildNonSpring()) {
				double median = livy.percentilesTD(kp, table, col, prob, 200).get()[0];
				final long end = System.currentTimeMillis();
				System.out.printf("Livy: %.4f (%d ms)%n", median, end -now);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		es.submit(() -> {
			final long now = System.currentTimeMillis();
			try (SparkExecutor direct = new DirectExecutor()) {
				double median = direct.percentilesTD(kp, table, col, prob, 200).get()[0];
				final long end = System.currentTimeMillis();
				System.out.printf("Direct: %.4f (%d ms)%n", median, end -now);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testCompareLivyDirectWithWarmup() throws Exception {
		String kp = "kp_test";
		String table = "usersession";
		String col = "duration";
		double[] prob = new double[] { 0.5 };

		SparkExecutor livy = LivyExecutor.buildNonSpring();
		SparkExecutor direct = new DirectExecutor();

		ExecutorService es = Executors.newWorkStealingPool();
		es.submit(() -> {
			try {
				livy.count(kp, table).get();
				final long now = System.currentTimeMillis();
				double median = livy.percentilesTD(kp, table, col, prob, 200).get()[0];
				final long end = System.currentTimeMillis();
				System.out.printf("Livy: %.4f (%d ms)%n", median, end - now);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					livy.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		es.submit(() -> {
			try {
				direct.count(kp, table).get();
				final long now = System.currentTimeMillis();
				double median = direct.percentilesTD(kp, table, col, prob, 200).get()[0];
				final long end = System.currentTimeMillis();
				System.out.printf("Direct: %.4f (%d ms)%n", median, end - now);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					direct.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testCompareLivyDirectWithWarmupTopK() throws Exception {
		String kp = "kp_test";
		String table = "usersession";
		String col = "city";

		SparkExecutor livy = LivyExecutor.buildNonSpring();
		SparkExecutor direct = new DirectExecutor();

		ExecutorService es = Executors.newWorkStealingPool();
		es.submit(() -> {
			try {
				livy.count(kp, table).get();
				final long now = System.currentTimeMillis();
				List<Counter<String>> top = livy.topKSpaceSaving(kp, table, col, 5, 200).get();
				final long end = System.currentTimeMillis();
				System.out.println("livy:");
				top.forEach(System.out::println);
				System.out.printf("took %d ms%n", end - now);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					livy.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		es.submit(() -> {
			try {
				direct.count(kp, table).get();
				final long now = System.currentTimeMillis();
				List<Counter<String>> top = direct.topKSpaceSaving(kp, table, col, 5, 200).get();
				final long end = System.currentTimeMillis();
				System.out.println("direct:");
				top.forEach(System.out::println);
				System.out.printf("took %d ms%n", end - now);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					direct.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		shutdownAndAwaitTermination(es);
	}

	@Test
	public void testLivyCaching() throws Exception {
		LivyExecutor livy1 = LivyExecutor.buildNonSpring();
		List<Counter<String>> list = livy1.topKSpaceSaving("kp_test", "usersession", "country", 5, 100).get();
		list.forEach(System.out::println);
		livy1.close();

		LivyExecutor livy2 = LivyExecutor.buildNonSpring();
		list = livy2.topKCountMinSketch("kp_test", "usersession", "country", 5, 100, 0.01, 0.99).get();
		list.forEach(System.out::println);
		livy2.close();
	}

}
