package com.dynatrace.msc.dore;

import com.dynatrace.msc.dore.domain.RealEstate;
import com.dynatrace.msc.dore.main.Registrator;
import com.dynatrace.msc.dore.query.Counter;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import org.junit.BeforeClass;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dynatrace.msc.dore.util.SparkConstants.PATH_PREFIX;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_MASTER;
import static com.dynatrace.msc.dore.util.SparkConstants.getTestJars;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractBaseTest implements Serializable {

	protected static final Map<String, String> CASSANDRA_OPTIONS;
	private static final long serialVersionUID = 1L;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("table", "test_data");
		tmp.put("keyspace", "kp_test");
		CASSANDRA_OPTIONS = unmodifiableMap(tmp);
	}

	protected static SparkConf conf;
	protected static SparkSession.Builder sessionBuilder;

	@BeforeClass
	public static void beforeClass() {
		conf = new SparkConf()
				.setMaster(SPARK_MASTER)
				.setJars(getTestJars())
				.set("spark.cassandra.connection.host", "localhost")
				.set("spark.eventLog.enabled", "true")
				// TODO: find a way to define the eventlog.dir property so that we can get insight to finished applications
//				.set("spark.eventLog.dir", "file:/tmp/spark-events")
//				.set("spark.history.fs.logDirectory", "file:/tmp/spark-events")
				.set("spark.eventLog.dir", "file:" + PATH_PREFIX + "/cluster/history")
				.set("spark.history.fs.logDirectory", "file:" + PATH_PREFIX + "/cluster/history")
				.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
				.set("spark.kryo.registrator", Registrator.class.getName())
				// disable dynamic allocation and set props for shuffling etc.
				.set("spark.shuffle.service.enabled", "false")
				.set("spark.dynamicAllocation.enabled", "false")
				.set("spark.io.compression.codec", "snappy")
				.set("spark.rdd.compress", "true")
				// configure executors
				.set("spark.executor.instances", "4")
				.set("spark.executor.cores", "8")
				.set("spark.pushdown", "true");

		sessionBuilder = SparkSession.builder()
				.master(SPARK_MASTER)
				.config(conf);
	}

	protected static void assertErrorSmallerThanEpsilon(final long approximate, final long exact, final double eps) {
		final double error = Math.abs(approximate - exact) / (double) exact;
		assertTrue(String.format("Error of %.4f exceeds eps of %.4f%n", error, eps), error < eps);
	}

	protected static void assertErrorSmallerThanEpsilon(final double approximate, final double exact, final double eps) {
		final double error = Math.abs(approximate - exact) / exact;
		assertTrue(String.format("Error of %f exceeds eps of %f%n", error, eps), error < eps);
	}

	protected static <T> JavaRDD<T> filter(JavaRDD<T> inputRdd, Function<T, Boolean> filterFun) {
		return inputRdd.filter(filterFun);
	}

	protected static JavaRDD<RealEstate> filterOnLat(JavaRDD<RealEstate> inputRdd) {
		Function<RealEstate, Boolean> func = new Function<RealEstate, Boolean>() {

			@Override
			public Boolean call(RealEstate estate) throws Exception {
				return estate.getLatitude() > 38.4f;
			}
		};
		JavaRDD<RealEstate> res = inputRdd.filter(func);
		return res;
	}

	public static void assertTop5TestCities(@Nonnull List<Counter<String>> counters) {
		requireNonNull(counters);
		assertEquals("sacramento", counters.get(0).getItem().toLowerCase());
		assertEquals("elk grove", counters.get(1).getItem().toLowerCase());
		assertEquals("lincoln", counters.get(2).getItem().toLowerCase());
		assertEquals("roseville", counters.get(3).getItem().toLowerCase());
		assertEquals("citrus heights", counters.get(4).getItem().toLowerCase());

		assertEquals(433, counters.get(0).getCount());
		assertEquals(108, counters.get(1).getCount());
		assertEquals(67, counters.get(2).getCount());
		assertEquals(46, counters.get(3).getCount());
		assertEquals(34, counters.get(4).getCount());
	}

}
