package com.dynatrace.msc.dore;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.allColumns;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.column;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapRowTo;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_MASTER;
import static org.apache.spark.sql.functions.col;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.spark.Partitioner;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.datastax.driver.core.Session;
import com.datastax.spark.connector.ColumnSelector;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.CassandraRow;
import com.datastax.spark.connector.japi.rdd.CassandraTableScanJavaRDD;
import com.datastax.spark.connector.util.JavaApiHelper;
import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.domain.City;
import com.dynatrace.msc.dore.domain.RealEstate;

public class CassandraConnectorTest extends AbstractBaseTest {

	private static JavaSparkContext sc;

	@BeforeClass
	public static void beforeClass() {
		AbstractBaseTest.beforeClass();
		sc = new JavaSparkContext(SPARK_MASTER, "CassandraConnectorTest", conf);

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		try (Session session = connector.openSession()) {
			session.execute("DROP TABLE IF EXISTS kp_test.top_cities");
			session.execute("CREATE TABLE kp_test.top_cities (city varchar PRIMARY KEY, count bigint)");
		}
	}

	@AfterClass
	public static void afterClass() {
		sc.close();
	}

	@Test
	public void testBasicFunctions() {
		CassandraTableScanJavaRDD<CassandraRow> csScan = javaFunctions(sc).cassandraTable("kp_test", "test_data");
		assertEquals(964, csScan.count());
		assertEquals(964, csScan.cassandraCount()); // cassandraCount
	}

	@Test
	public void testWriteTopK() {
		JavaRDD<String> cityRdd = javaFunctions(sc).cassandraTable("kp_test", "test_data")
				.select("city") // projection is done on the cassandra node
				.map(row -> row.getString("city"));

		// calculate a StreamSummary for each partition
		// -> the calculations should be done locally on the nodes
		// TODO: check data locality
		StreamSummary<String> summary = cityRdd.mapPartitions(it -> {
			StreamSummary<String> ss = new StreamSummary<>(20);
			while (it.hasNext()) {
				ss.offer(it.next());
			}
			return Collections.singleton(ss).iterator();
		}).reduce(StreamSummary::merge);

		List<City> topCities = summary.topK(4)
				.stream()
				.map(c -> new City(c.getItem(), c.getCount()))
				.sorted(Comparator.comparingLong(City::getCount).reversed())
				.collect(Collectors.toList());

		assertEquals(4, topCities.size());

		assertThat(topCities.get(0).getCity(), equalToIgnoringCase("sacramento"));
		assertEquals(433, topCities.get(0).getCount());

		assertThat(topCities.get(1).getCity(), equalToIgnoringCase("elk grove"));
		assertEquals(108, topCities.get(1).getCount());

		assertThat(topCities.get(2).getCity(), equalToIgnoringCase("lincoln"));
		assertEquals(67, topCities.get(2).getCount());

		assertThat(topCities.get(3).getCity(), equalToIgnoringCase("roseville"));
		assertEquals(46, topCities.get(3).getCount());

		topCities.forEach(System.out::println);

		JavaRDD<City> topCityRdd = sc.parallelize(topCities);
		javaFunctions(topCityRdd).writerBuilder("kp_test", "top_cities", mapToRow(City.class)).saveToCassandra();

		CassandraTableScanJavaRDD<City> topCityRddFromCassandra = javaFunctions(sc).cassandraTable("kp_test", "top_cities", mapRowTo(City.class));
		List<City> topCitiesReadFromCassandra = topCityRddFromCassandra.collect();
		topCitiesReadFromCassandra.forEach(city -> assertTrue(topCities.contains(city)));
//		System.out.println();
//		topCitiesReadFromCassandra.forEach(System.out::println);
	}

	@Test
	public void testMapRowToBean() {
		CassandraTableScanJavaRDD<RealEstate> rdd = javaFunctions(sc).cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class));
		JavaRDD<RealEstate> distinctCityRdd = rdd.select("city").distinct();

		SQLContext sqlCtx = new SQLContext(SparkSession.builder().sparkContext(sc.sc()).getOrCreate());
		Dataset<Row> data = sqlCtx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

		assertEquals(39, distinctCityRdd.count());
		assertEquals(39, sqlCtx.sql("select distinct city from data").count());

		List<RealEstate> distinctCities = distinctCityRdd.collect();
		assertEquals(39, distinctCities.size());
		distinctCities.forEach(city -> {
			assertNotNull(city.getCity());
			assertNull(city.getStreet());
			assertNull(city.getState());
		});

		assertEquals(964, rdd.count());
	}

	@Ignore
	@Test
	public void testSparkSqlWithCassandraSparkConnector() {
		SQLContext sqlContext = new SQLContext(sessionBuilder.getOrCreate());
		Dataset<Row> data = sqlContext.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
		data.createOrReplaceTempView("data");

//		sqlContext.sql("CREATE TEMPORARY VIEW data USING " + SPARK_CASSANDRA_FORMAT + " OPTIONS (table 'test_data', keyspace 'kp_test')");

		// need to cast 38.4 as float or else it won't get pushed down to the node
		// as it would be interpreted as double
		Dataset<Row> sqlDs = sqlContext.sql("SELECT city FROM data WHERE latitude > cast(38.4 as float)");

		System.out.println();

		Dataset<Row> apiDs = data.filter("latitude > cast(38.4 as float)").select(col("city"));

//		String sqlDebug = sqlDs.javaRDD().toDebugString();
//		String apiDebug = apiDs.javaRDD().toDebugString();
//		System.out.println(sqlDebug);
//		System.out.println(apiDebug);

		CassandraTableScanJavaRDD<RealEstate> csRdd = javaFunctions(sc).cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class));

//		NOTE: we need to move any lambda to an own function
//		or else it won't get serialized correctly in the tests
		JavaRDD<RealEstate> filtered = filterOnLat(csRdd);

		Dataset<RealEstate> filteredDs = sqlContext.sql("SELECT * FROM data WHERE latitude > cast(38.4 as float)").as(Encoders.bean(RealEstate.class));

		List<RealEstate> cassandraEstates = filtered.collect();
		List<RealEstate> sqlEstates = filteredDs.collectAsList();

		assertThat(cassandraEstates.size(), equalTo(sqlEstates.size()));
		assertEquals(sqlEstates, cassandraEstates);
	}

	@Test
	public void testCassandraPartitioner() {
		JavaPairRDD<Float, RealEstate> keyedRdd = javaFunctions(sc)
				.cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class))
				.keyBy(RealEstate::getLatitude);

		assertEquals(964, keyedRdd.values().collect().size());

		System.out.println(keyedRdd.groupByKey().toDebugString());

//		JavaPairRDD<Float, RealEstate> untypedRdd = javaFunctions(sc)
//				.cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class))
//				.keyBy(mapRowTo(RealEstate.class), mapToRow(RealEstate.class), Float.class, "latitude")

		JavaPairRDD<Float, Iterable<RealEstate>> untypedRdd = javaFunctions(sc)
				.cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class))
				.spanBy(RealEstate::getLatitude, Float.class);

		System.out.println(untypedRdd.groupByKey().toDebugString());

//		Partitioner partitioner = keyedRdd.partitioner().orElse(null);
//		assertNotNull(partitioner);
//
//		System.out.println(partitioner.numPartitions());
//		System.out.println(partitioner);
	}

	@Test
	public void testRepartitionByCassandraReplica() {
		CassandraTableScanJavaRDD<RealEstate> scan = javaFunctions(sc)
				.cassandraTable("kp_test", "test_data", mapRowTo(RealEstate.class));

		JavaRDD<RealEstate> repartitionedRdd = javaFunctions(scan)
				.repartitionByCassandraReplica("kp_test", "test_data", 10, CassandraJavaUtil.someColumns("latitude"), mapToRow(RealEstate.class));

//		System.out.println("repartitioning:");
//		System.out.println(repartitionedRdd.toDebugString());

		int sum = repartitionedRdd.mapPartitionsWithIndex((i, it) -> {
			int count = 0;
			System.out.println("in partition " + i);
			for (; it.hasNext(); it.next()) {
				count++;
			}
			return Collections.singleton(count).iterator();
		}, true).reduce(Integer::sum);

		System.out.println("had " + sum + " rows");
	}

}
