package com.dynatrace.msc.dore;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.min;

import java.util.Collections;

import org.HdrHistogram.DoubleHistogram;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.util.sketch.CountMinSketch;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.dynatrace.msc.dore.domain.RealEstate;
import com.tdunning.math.stats.TDigest;

public class AlgoTest extends AbstractBaseTest {

	private static SQLContext ctx;
	private static Dataset<Row> data;

	@BeforeClass
	public static void beforeClass() {
		AbstractBaseTest.beforeClass();
		ctx = new SQLContext(sessionBuilder.getOrCreate());
		data = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load();
	}

	@Test
	public void testApproxCountDistinct() {
		for (int i = 0; i  < 5; i++) {
			JavaRDD<Row> cityRdd = data.select(col("city")).javaRDD();

			long countApprox = cityRdd.countApproxDistinct(0.1);
			long count = data.select("city").distinct().count();

			assertErrorSmallerThanEpsilon(countApprox, count, 0.1);

			countApprox = cityRdd.countApproxDistinct(0.5);
			assertErrorSmallerThanEpsilon(countApprox, count, 0.5);
		}
	}

	@Test
	public void testPercentilesHdrHistogram() {
//		for (int j = 0; j < 5; j++) {
			double[] approxQuantiles = data.stat().approxQuantile("latitude", new double[] { 0.1, 0.5, 0.99 }, 0.01);

			DoubleHistogram histogram = data.select(col("latitude")).toJavaRDD().mapPartitions(it -> {
				DoubleHistogram histo = new DoubleHistogram(5);
				while (it.hasNext()) {
					histo.recordValue(it.next().getFloat(0));
				}
				return Collections.singleton(histo).iterator();
			}).reduce((h1, h2) -> {
				h1.add(h2);
				return h1;
			});

			double[] hdrQuantiles = new double[3];
			hdrQuantiles[0] = histogram.getValueAtPercentile(0.1);
			hdrQuantiles[1] = histogram.getValueAtPercentile(0.5);
			hdrQuantiles[2] = histogram.getValueAtPercentile(0.99);

			System.out.printf("0.1  -> GK %.2f HDR %.2f%n", approxQuantiles[0], hdrQuantiles[0]);
			System.out.printf("0.5  -> GK %.2f HDR %.2f%n", approxQuantiles[1], hdrQuantiles[1]);
			System.out.printf("0.99 -> GK %.2f HDR %.2f%n", approxQuantiles[2], hdrQuantiles[2]);

			for (int i = 0; i < 3; i++) {
				assertErrorSmallerThanEpsilon(approxQuantiles[i], hdrQuantiles[i], 0.02);
			}
//		}
	}

	@Test
	@Ignore // AVLTreeDigest not serializable
	public void testPercentilesTDigest() {
		double[] approxQuantiles = data.stat().approxQuantile("latitude", new double[] { 0.1, 0.5, 0.99 }, 0.1);

		TDigest tDigest = data.select(col("latitude")).toJavaRDD().mapPartitions(it -> {
			TDigest digest = TDigest.createDigest(200);
			while (it.hasNext()) {
				digest.add(it.next().getFloat(0));
			}
			return Collections.singleton(digest).iterator();
		}).reduce((td1, td2) -> {
			td1.add(td2);
			return td1;
		});

		double[] tdQuantiles = new double[3];
		tdQuantiles[0] = tDigest.quantile(0.1);
		tdQuantiles[1] = tDigest.quantile(0.5);
		tdQuantiles[2] = tDigest.quantile(0.99);

		System.out.printf("0.1  -> GK %.2f HDR %.2f%n", approxQuantiles[0], tdQuantiles[0]);
		System.out.printf("0.5  -> GK %.2f HDR %.2f%n", approxQuantiles[1], tdQuantiles[1]);
		System.out.printf("0.99 -> GK %.2f HDR %.2f%n", approxQuantiles[2], tdQuantiles[2]);

		for (int i = 0; i < 3; i++) {
			assertErrorSmallerThanEpsilon(approxQuantiles[i], tdQuantiles[i], 0.1);
		}
	}

	@Test
	@Ignore // the lambda function used in filter() is not compiled yet
	public void testCountMinSketch() {
		double eps = 0.1; double confidence = 0.9; int seed = 42;
		CountMinSketch cms = data.stat().countMinSketch(col("city"), eps, confidence, seed);

		long countApprox = cms.estimateCount("SACRAMENTO");
		long count = data.select(col("city"))
				.filter(c -> c.getString(0).equalsIgnoreCase("sacramento"))
				.count();

		assertErrorSmallerThanEpsilon(countApprox, count, eps);
	}

	@Test
	public void testCompareAggregateVsSql() {
		data.createOrReplaceTempView("data");

		Dataset<Row> minDs = data.select(min(col("baths")));
		Dataset<Row> minDsSql = ctx.sql("SELECT MIN(baths) FROM data");

		System.out.println("agg: " + minDs.collectAsList().get(0).getInt(0));
		System.out.println("sql: " + minDsSql.collectAsList().get(0).getInt(0));

		Dataset<Long> sumBathDs = ctx.sql("SELECT SUM(baths) FROM data").as(Encoders.LONG());
		System.out.println(sumBathDs.first());
	}

	@Test
	public void testPartitioner() {
		Dataset<RealEstate> typedDs = ctx.read().format(SPARK_CASSANDRA_FORMAT).options(CASSANDRA_OPTIONS).load()
				.as(Encoders.bean(RealEstate.class));

		typedDs.groupByKey(RealEstate::getLatitude, Encoders.FLOAT());
	}

}
