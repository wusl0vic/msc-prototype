package com.dynatrace.msc.dore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.dynatrace.msc.dore.algorithm.CountMinSketchX;
import com.dynatrace.msc.dore.algorithm.StreamSummary;

public class AlgoTestNonSpark {

	@Test
	public void testCMSX() {
		CountMinSketchX cms = new CountMinSketchX(0.001, 0.9, 42, 5);
		StreamSummary<String> summary = new StreamSummary<>(5);

		String[] stream = { "AT", "AT", "DE", "AT", "JP", "DE", "AT", "JP", "AUS", "IRE", "JP", "USA", "AT", "JP" };

		for (String s : stream) {
			cms.add(s, 1);
			summary.offer(s);
		}

		Map<String, Long> topCMS = cms.topK(3);
		topCMS.entrySet().forEach(System.out::println);

		System.out.println();

		List<StreamSummary<String>.Counter<String>> topSpaceSaving = summary.topK(3);
		topSpaceSaving.forEach(System.out::println);

		assertTrue(topCMS.containsKey("AT"));
		assertTrue(topCMS.containsKey("JP"));
		assertTrue(topCMS.containsKey("DE"));

		assertEquals(5, topCMS.get("AT").longValue());
		assertEquals(4, topCMS.get("JP").longValue());
		assertEquals(2, topCMS.get("DE").longValue());

		assertEquals(5, topSpaceSaving.get(0).getCount());
		assertEquals(4, topSpaceSaving.get(1).getCount());
		assertEquals(2, topSpaceSaving.get(2).getCount());
	}

	@Test
	public void testMergeCMSX() {
		CountMinSketchX cms1 = new CountMinSketchX(0.001, 0.9, 42, 5);
		CountMinSketchX cms2 = new CountMinSketchX(0.001, 0.9, 42, 5);

		String[] stream1 = { "AT", "AT", "DE", "AT", "JP", "DE", "AT", "JP", "AUS", "IRE", "JP", "USA", "AT", "JP" };
		String[] stream2 = { "AT", "AUS", "IRE" };

		for (String s : stream1) {
			cms1.add(s, 1);
		}

		for (String s : stream2) {
			cms2.add(s, 1);
		}

		CountMinSketchX merged = cms1.mergeX(cms2);
		Map<String, Long> top = merged.topK(3);
		top.entrySet().forEach(System.out::println);
	}

}
