package com.dynatrace.msc.dore.livy;

import static com.dynatrace.msc.dore.AbstractBaseTest.assertTop5TestCities;
import static java.util.Collections.unmodifiableMap;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.spark.sql.Encoders;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.dynatrace.msc.dore.algorithm.StreamSummary;
import com.dynatrace.msc.dore.query.Counter;

public class LivyExecutorTest {

	static final Map<String, String> TEST_OPTIONS;

	static {
		Map<String, String> tmp = new HashMap<>();
		tmp.put("keyspace", "kp_test");
		tmp.put("table", "test_data");
		TEST_OPTIONS = unmodifiableMap(tmp);
	}

	private static LivyExecutor staticExecutor;

	@BeforeClass
	public static void beforeClass() throws Exception {
		staticExecutor = LivyExecutor.buildNonSpring();
//		staticExecutor.setCassandraOptions(TEST_OPTIONS);

		// submit an initial job to cache the dataset
		// to better be able to compare runtimes
//		staticExecutor.count().get();
	}

	@AfterClass
	public static void afterClass() {
		staticExecutor.stop(true);
	}

	@Test
	public void testMax() throws Exception {
		CompletableFuture<Long> maxF = staticExecutor.max(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"), "baths");
		long maxBaths = maxF.get();
		assertEquals(5, maxBaths);
		System.out.println(maxBaths);
	}

	@Test
	public void testAvg() throws Exception {
		double avgBaths = staticExecutor.avg(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"baths").get();
		double avgBeds = staticExecutor.avg(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"beds").get();

		System.out.printf("Avg baths: %.4f%n", avgBaths);
		System.out.printf("Avg beds:  %.4f%n", avgBeds);
	}

	@Test
	public void testTopKSpaceSaving() throws Exception {
		CompletableFuture<List<Counter<String>>> future = staticExecutor.topKSpaceSaving(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city",
				5, 60, Encoders.STRING());
		List<Counter<String>> topCities = future.get();

		System.out.println();
		topCities.forEach(System.out::println);
		assertTop5TestCities(topCities);
	}

	@Test
	public void testTopKCMS() throws Exception {
		CompletableFuture<List<Counter<String>>> future = staticExecutor.topKCountMinSketch(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city",
				5, 60, 0.001, 0.99);
		List<Counter<String>> topCities = future.get();


		topCities.forEach(System.out::println);
		assertTop5TestCities(topCities);
	}

	@Test
	public void testTopKExact() throws Exception {
		CompletableFuture<List<Counter<String>>> future = staticExecutor.topKExact(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city", 5);
		List<Counter<String>> topCities = future.get();

		topCities.forEach(System.out::println);
		assertTop5TestCities(topCities);
	}

	@Test
	public void testCompareTopK() throws Exception {
		long rows = staticExecutor.count(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table")).get();
		System.out.printf("Got %d rows%n%n", rows);

		long now = System.currentTimeMillis();
		CompletableFuture<List<Counter<String>>> spaceSavingFuture = staticExecutor.topKSpaceSaving(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city", 5, 50);
		CompletableFuture<List<Counter<String>>> cmsFuture = staticExecutor.topKCountMinSketch(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city", 5, 20, 0.001, 0.99);
		CompletableFuture<List<Counter<String>>> exactFuture = staticExecutor.topKExact(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city", 5);

		spaceSavingFuture.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Space Saving:");
			top.forEach(System.out::println);
			System.out.printf("took %d ms%n%n", end - now);
			assertTop5TestCities(top);
		});

		cmsFuture.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Count Min Sketch:");
			top.forEach(System.out::println);
			System.out.printf("took %d ms%n%n", end - now);
			assertTop5TestCities(top);
		});

		exactFuture.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Exact (Spark SQL):");
			top.forEach(System.out::println);
			System.out.printf("took %d ms%n%n", end - now);
			assertTop5TestCities(top);
		});

		CompletableFuture.allOf(spaceSavingFuture, cmsFuture, exactFuture).join();
	}

	@Ignore // serialization issues with StreamSummary.bucketList -> Bucket.counters
	@Test
	public void testStreamSummary() throws Exception {
		LivyExecutor livy = LivyExecutor.buildNonSpring();

		CompletableFuture<StreamSummary<String>> future = livy.streamSummary(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"city", 50, Encoders.STRING());
		StreamSummary<String> summary = future.get();
		System.out.println("Top 1 City:");
		summary.topK(1).forEach(System.out::println);

		System.out.println();
		System.out.println("Top 10 Cities:");
		summary.topK(10).forEach(System.out::println);

		livy.stop(true);
	}

	@Test
	public void testMedian() throws Exception {
		double median = staticExecutor.median(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"baths").get();
		System.out.printf("median of baths = %.4f%n", median);

		median = staticExecutor.median(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude").get();
		double medianGK = staticExecutor.percentilesGK(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude", new double[] { 0.5 }, 0.0).get()[0];

		System.out.printf("median: %.4f; gk: %.4f%n", median, medianGK);
	}

	@Test
	public void testQuantileAlgos() throws Exception {
		double[] probabilities = new double[] { 0.1, 0.5 };
		long now = System.currentTimeMillis();

		CompletableFuture<double[]> gkFuture = staticExecutor.percentilesGK(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude", probabilities, 0.0);
		CompletableFuture<double[]> tdFuture = staticExecutor.percentilesTD(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude", probabilities, 100);
		CompletableFuture<double[]> hdrFuture = staticExecutor.percentilesHDR(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),
				"latitude", probabilities, 5, 1000);
		CompletableFuture<Double> medianFuture = staticExecutor.median(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude");

		gkFuture.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("GK:  0.1: %.4f, 0.5: %.4f (took %d ms)%n", p[0], p[1], end - now);
		});

		tdFuture.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("TD:  0.1: %.4f, 0.5: %.4f (took %d ms)%n", p[0], p[1], end - now);
		});

		hdrFuture.thenAccept(p -> {
			long end = System.currentTimeMillis();
			System.out.printf("HDR: 0.1: %.4f, 0.5: %.4f (took %d ms)%n", p[0], p[1], end - now);
		});

		medianFuture.thenAccept(m -> {
			long end = System.currentTimeMillis();
			System.out.printf("Median: %.4f (took %d ms)%n", m, end - now);
		});

		CompletableFuture.allOf(gkFuture, tdFuture, hdrFuture, medianFuture).join();
	}

	@Test
	public void testCompareMedianSync() throws Exception {
		long now = System.currentTimeMillis();
		double median = staticExecutor.median(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude").get();
		long end = System.currentTimeMillis();
		System.out.printf("Median: %.4f (took %d ms)%n", median, end - now);

		now = System.currentTimeMillis();
		median = staticExecutor.percentilesGK(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude", new double[] { 0.5 }, 0.0).get()[0];
		end = System.currentTimeMillis();
		System.out.printf("GK: %.4f (took %d ms)%n", median, end - now);

		now = System.currentTimeMillis();
		median = staticExecutor.percentilesTD(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),"latitude", new double[] { 0.5 }, 200).get()[0];
		end = System.currentTimeMillis();
		System.out.printf("TD: %.4f (took %d ms)%n", median, end - now);

		now = System.currentTimeMillis();
		median = staticExecutor.percentilesHDR(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table"),
				"latitude", new double[] { 0.5 }, 5, 1000).get()[0];
		end = System.currentTimeMillis();
		System.out.printf("HDR: %.4f (took %d ms)%n", median, end - now);
	}

	@Test
	public void testCount() throws Exception {
		long count = staticExecutor.count(TEST_OPTIONS.get("keyspace"), TEST_OPTIONS.get("table")).get();
		System.out.println("Count: " + count);
	}

}
