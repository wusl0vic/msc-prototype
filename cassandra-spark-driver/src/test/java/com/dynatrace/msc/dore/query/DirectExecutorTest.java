package com.dynatrace.msc.dore.query;

import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_CASSANDRA_FORMAT;
import static com.dynatrace.msc.dore.util.SparkConstants.SPARK_MASTER;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.col;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

import com.dynatrace.msc.dore.AbstractBaseTest;

public class DirectExecutorTest extends AbstractBaseTest {

	private static final String KP = "kp_test";
	private static final String TABLE = "usersession";

	private DirectExecutor executor = new DirectExecutor(SPARK_MASTER, conf);

	private Map<String, String> options = new HashMap<>();

	@Test
	public void testPercentiles() throws Exception {
		double[] prob = new double[] { 0.5 };
		long maxDuration = executor.max(KP, TABLE, "duration").get();
		final long now = System.currentTimeMillis();
		CompletableFuture<double[]> f2 = executor.percentilesHDR(KP, TABLE, "duration", prob, 5, maxDuration);
		CompletableFuture<double[]> f3 = executor.percentilesGK(KP, TABLE, "duration", prob, 0.0);
		CompletableFuture<double[]> f1 = executor.percentilesTD(KP, TABLE, "duration", prob, 200);
		CompletableFuture<Double> f4 = executor.median(KP, TABLE, "duration");

		f1.thenAccept(p -> {
			final long end = System.currentTimeMillis();
			System.out.printf("TD:    %.4f (%d ms)%n", p[0], end - now);
		});

		f2.thenAccept(p -> {
			final long end = System.currentTimeMillis();
			System.out.printf("HDR:   %.4f (%d ms)%n", p[0], end - now);
		});

		f3.thenAccept(p -> {
			final long end = System.currentTimeMillis();
			System.out.printf("GK:    %.4f (%d ms)%n", p[0], end - now);
		});

		f4.thenAccept(m -> {
			final long end = System.currentTimeMillis();
			System.out.printf("exact: %.4f (%d ms)%n", m, end - now);
		});

		CompletableFuture.allOf(f2, f3, f1, f4).join();
	}

	@Test
	public void testMedian() {
		options.put("keyspace", KP);
		options.put("table", TABLE);
		SparkSession session = sessionBuilder.getOrCreate();
		Dataset<Row> ds = session.read().format(SPARK_CASSANDRA_FORMAT)
				.options(options).load();

		String col = "duration";

		List<Row> asdf = ds.filter(col(col).isNotNull()).select(col(col)).sort(asc(col)).collectAsList();
		double val = Double.valueOf(asdf.get(asdf.size() / 2).get(0).toString());
		System.out.println("val = " + val);

		val = (Double.valueOf(asdf.get(asdf.size() / 2).get(0).toString()) + Double.valueOf(asdf.get(asdf.size() / 2 - 1).get(0).toString())) / 2;
		System.out.println("val = " + val);
	}

	@Test
	public void testPercentilesSequential() throws Exception {
		double[] prob = new double[] { 0.5 };
		long maxDuration = executor.max(KP, TABLE, "duration").get();

		StopWatch sw = StopWatch.createStarted();
		double[] p = executor.percentilesGK(KP, TABLE, "duration", prob, 0.0).get();
		long took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("GK:      %.4f (%d ms)%n", p[0], took);

		sw.reset(); sw.start();
		p = executor.percentilesHDR(KP, TABLE, "duration", prob, 5, maxDuration).get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("HDR:     %.4f (%d ms)%n", p[0], took);

		sw.reset(); sw.start();
		p = executor.percentilesTD(KP, TABLE, "duration", prob, 200).get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("TD:      %.4f (%d ms)%n", p[0], took);

		sw.reset(); sw.start();
		double median = executor.median(KP, TABLE, "duration").get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("exact:   %.4f (%d ms)%n", median, took);
	}

	@Test
	public void testTopK() throws Exception {
		executor.count(KP, TABLE).get();
		long now = System.currentTimeMillis();
		CompletableFuture<List<Counter<String>>> future1 = executor.topKSpaceSaving(KP, TABLE, "city", 10, 200);
		future1.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Space saving: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture<List<Counter<String>>> future2 = executor.topKExact(KP, TABLE, "city", 10);
		future2.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("Exact: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture<List<Counter<String>>> future3 = executor.topKCountMinSketch(KP, TABLE, "city", 10, 200, 0.01, 0.99);
		future3.thenAccept(top -> {
			long end = System.currentTimeMillis();
			System.out.println("CMS: ");
			top.forEach(System.out::println);
			System.out.println("took " + (end - now) + " ms");
			System.out.println();
		});

		CompletableFuture.allOf(future1, future2, future3).join();
	}

	@Test
	public void testTopKSequential() throws Exception {
		executor.count(KP, TABLE).get();

		StopWatch sw = StopWatch.createStarted();
		List<Counter<String>> counts = executor.topKExact(KP, TABLE, "city", 10).get();
		long took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.println("Exact: ");
		counts.forEach(System.out::println);
		System.out.println("took " + took + " ms");
		System.out.println();

		sw.reset(); sw.start();
		counts = executor.topKSpaceSaving(KP, TABLE, "city", 10, 200).get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.println("SpaceSaving: ");
		counts.forEach(System.out::println);
		System.out.println("took " + took + " ms");
		System.out.println();

		sw.reset();sw.start();
		counts = executor.topKCountMinSketch(KP, TABLE, "city", 10, 200, 0.01, 0.99).get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.println("CMS: ");
		counts.forEach(System.out::println);
		System.out.println("took " + took + " ms");

		sw.stop();
	}

	@Test
	public void testCountDistinct() throws Exception {
		executor.count(KP, TABLE).get();

		StopWatch sw = StopWatch.createStarted();
		long cardinality = executor.cardinality(KP, TABLE, "city", 0.01, true).get();
		long took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("Exact: %d (took %d ms)%n", cardinality, took);

		sw.reset(); sw.start();
		cardinality = executor.cardinality(KP, TABLE, "city", 0.001, false).get();
		took = sw.getTime(TimeUnit.MILLISECONDS);
		System.out.printf("HLL: %d (took %d ms)%n", cardinality, took);
	}

}
